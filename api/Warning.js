import { API_URL } from '../constants/Env'

const url = API_URL
const headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
}

export function addWarning($req) {
    return fetch(`${url}/warnings`, {
        method:'POST',
        headers: headers,
        body: JSON.stringify($req)
    })
        .then(async res => {
            const json = await res.json()
            return json
        })
        .catch(err => console.log(err))
}

export function getAllWarnings() {
    return fetch(`${url}/warnings`)
        .then(async res => {
            const jsonArr = await res.json()
            return jsonArr
        })
        .catch(err => console.error(err))
}
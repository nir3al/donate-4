import { API_URL } from '../constants/Env'

const url = API_URL
const headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
}

export function sendRequest($req) {
    return fetch(`${url}/requests`, {
        method:'POST',
        headers: headers,
        body: JSON.stringify($req)
    })
        .then(async res => {
            const json = await res.json()
            return json
        })
        .catch(err => console.log(err))
}

export function getAllRequests() {
    return fetch(`${url}/requests`)
        .then(async res => {
            const jsonArr = await res.json()
            return jsonArr
        })
        .catch(err => console.log(err))
}

export function getRequest(id) {
    return fetch(`${url}/requests/${id}`)
        .then(async res => {
            const json = await res.json()
            return json
        })
        .catch(err => console.log(err))
}

export function updateRequest($req) {
    return fetch(`${url}/requests/accept`, {
        method:'PUT',
        headers: headers,
        body: JSON.stringify($req)
    })
        .then(async res => {
            const json = await res.json()
            return json
        })
        .catch(err => console.log(err))
}

export function finishRequest($user) {
    console.log(JSON.stringify($user))
    return fetch(`${url}/users/received`, {
        method:'PUT',
        headers: headers,
        body: JSON.stringify($user)
    })
        .then(async res => {
            const json = await res.json()
            return json
        })
        .catch(err => console.log(err))
}
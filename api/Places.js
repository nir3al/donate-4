import { GOOGLE_MAPS_API_URL } from '../constants/Env'

const url = GOOGLE_MAPS_API_URL

export function getPlaceData(placeId) {
    return fetch(`${url}${placeId}`)
        .then(async res => {
            const jsonArr = await res.json()
            return jsonArr
        })
        .catch(err => console.error(err))
}
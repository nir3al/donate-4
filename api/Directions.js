import { GOOGLE_MAPS_DIRECTIONS_API_KEY } from '../constants/Env'

// import polyline from "@mapbox/polyline";
// import decodePolyline from "decode-google-map-polyline";

const mode = 'driving'; // 'walking';

const APIKEY = GOOGLE_MAPS_DIRECTIONS_API_KEY;

function getUrl(origin, destination) {
    return `https://maps.googleapis.com/maps/api/directions/json?origin=${origin}&destination=${destination}&key=${APIKEY}&mode=${mode}`;
}

// function decode(t,e){for(var n,o,u=0,l=0,r=0,d= [],h=0,i=0,a=null,c=Math.pow(10,e||5);u<t.length;){a=null,h=0,i=0;do a=t.charCodeAt(u++)-63,i|=(31&a)<<h,h+=5;while(a>=32);n=1&i?~(i>>1):i>>1,h=i=0;do a=t.charCodeAt(u++)-63,i|=(31&a)<<h,h+=5;while(a>=32);o=1&i?~(i>>1):i>>1,l+=n,r+=o,d.push([l/c,r/c])}return d=d.map(function(t){return{latitude:t[0],longitude:t[1]}})}
// transforms something like this geocFltrhVvDsEtA}ApSsVrDaEvAcBSYOS_@... to an array of coordinates

// function decode(encoded){
//     // array that holds the points
//     var points=[ ]
//     var index = 0, len = encoded.length;
//     var lat = 0, lng = 0;

//     while (index < len) {
//         var b, shift = 0, result = 0;

//         do {
//             b = encoded.charAt(index++).charCodeAt(0) - 63;//finds ascii                                                                                    //and substract it by 63
//             result |= (b & 0x1f) << shift;
//             shift += 5;
//         } while (b >= 0x20);


//         var dlat = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
//         lat += dlat;
//         shift = 0;
//         result = 0;

//         do {
//             b = encoded.charAt(index++).charCodeAt(0) - 63;
//             result |= (b & 0x1f) << shift;
//             shift += 5;
//         } while (b >= 0x20);

//         var dlng = ((result & 1) != 0 ? ~(result >> 1) : (result >> 1));
//         lng += dlng;
    
//         points.push({latitude:( lat / 1E5),longitude:( lng / 1E5)})  
//     }

//     return points
// }

export function getRoute(origin, destination) {
    return fetch(getUrl(origin, destination))
        .then(response => {
            const json = response.json();
            console.log(json);
            return json;
        })
        .then(responseJson => {
            console.log('map direction coords response json', responseJson);
            // if (responseJson.routes.length) {
            //     this.setState({
            //         coords: decode(responseJson.routes[0].overview_polyline.points) // definition below
            //     });
            // }
            if (responseJson.routes.length) {
                // const encoded = responseJson.routes[0].overview_polyline.points;
                const _coords = responseJson.routes[0].legs[0].steps;
                const coords = _coords.map(step => {
                    return {
                        latitude:step.end_location.lat, 
                        longitude:step.end_location.lng
                    };
                });
                // console.log('encoded', encoded);
                // const _points = decodePolyline(encoded).map(latlong => {latitude:latlong.lat, longitude:latlong.lng});
                // const _points = polyline.decode(encoded).map(x => {return { latitude:x[0], longitude:x[1] }});
                // const _points = 
                console.log(coords);
                return coords;
            } else {
                return [];
            }
        }).catch(e => {console.warn(e)});
}
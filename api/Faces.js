import { API_URL } from '../constants/Env'

const url = API_URL
const headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
}

export function getMatchForFace(faceId, locName) {
    return fetch(`${url}/faces/do/match`, {
        method:'POST',
        headers: headers,
        body: JSON.stringify({faceId,locName,})
    })
        .then(async res => {
            const json = await res.json()
            return json
        })
        .catch(err => console.log(err))
}
import { API_URL } from '../constants/Env'

const url = API_URL
const headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
}

export function signUp(name, username, password) {
    return fetch(`${url}/auth/register`, {
        method:'POST',
        headers: headers,
        body: JSON.stringify({
            "name": name,
            "username": username,
            "password": password
        })
    })
        .then(async res => {
            const json = await res.json()
            const userObj = json || null
            return userObj
        }).catch(err => console.log(err))
}

export function logIn(username, password) {
    return fetch(`${url}/auth/login`, {
        method:'POST',
        headers: headers,
        body: JSON.stringify({
            "username": username,
            "password": password
        })
    })
        .then(async res => {
            const json = await res.json()
            const userObj = json || null
            return userObj
        }).catch(err => console.log(err))
}

export function getUser(userId) {
    console.log(JSON.stringify(userId))
    return fetch(`${url}/auth/users/${userId+''}`, {
        method:'GET',
    })
        .then(async res => {
            const json = await res.json()
            const userObj = json || null
            return userObj
        }).catch(err => console.log(err))
}
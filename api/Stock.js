import { API_URL } from '../constants/Env'

const url = API_URL
const headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
}

export function updateStock($stock) {
    return fetch(`${url}/stocks`, {
        method:'PUT',
        headers: headers,
        body: JSON.stringify($stock)
    })
        .then(async res => {
            const json = await res.json()
            const obj = json || null
            return obj
        }).catch(err => console.log(err))
}

export function getItems(reqId) {
    return fetch(`${url}/stocks/request/${reqId}`, {
        method:'GET',
    })
        .then(async res => {
            const json = await res.json()
            const obj = json || null
            return obj
        }).catch(err => console.log(err))
}

export function checkStock($id) {
    return fetch(`${url}/stocks/check/${$id}`)
        .then(async res => {
            const json = await res.json()
            const obj = json || null
            return obj
        }).catch(err => console.log(err))
}
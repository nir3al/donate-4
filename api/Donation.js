import { API_URL } from '../constants/Env'

const url = API_URL
const headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
}

export function addDonation($req) {
    return fetch(`${url}/donations`, {
        method:'POST',
        headers: headers,
        body: JSON.stringify($req)
    })
        .then(async res => {
            const json = await res.json()
            return json
        })
        .catch(err => console.log(err))
}

export function addDonationInBulk($req) {
    return fetch(`${url}/donations/bulk`, {
        method:'POST',
        headers: headers,
        body: JSON.stringify($req)
    })
        .then(async res => {
            const json = await res.json()
            return json
        })
        .catch(err => console.log(err))
}

export function getAllDonations() {
    return fetch(`${url}/donations`)
        .then(async res => {
            const jsonArr = await res.json()
            return jsonArr
        })
        .catch(err => console.log(err))
}
import { DARK_SKY_API_URL, API_URL } from '../constants/Env'

const url_darksky = DARK_SKY_API_URL
const api_url = API_URL

export function getWeatherData(lat,long) {
    return fetch(`${url_darksky}${lat},${long}`)
        .then(async res => {
            const jsonArr = await res.json()
            return jsonArr
        })
        .catch(err => console.error(err))
}

export function getRainfallOfWeek(yy,mm,dd) {
    return fetch(`${api_url}/rainfall/week/${yy}/${mm}/${dd}`)
        .then(async res => {
            const jsonArr = await res.json()
            return jsonArr
        })
        .catch(err => console.error(err))
}

export function getRainfallOfDay(yy,mm,dd) {
    return fetch(`${api_url}/rainfall/${yy}/${mm}/${dd}`)
        .then(async res => {
            const jsonObj = await res.json()
            return jsonObj
        })
        .catch(err => console.error(err))
}
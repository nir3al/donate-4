import { 
    API_URL,
    IMGUR_API_URL_IMAGE,
    IMGUR_API_HEADER_AUTHORIZATION,
    IMGUR_API_HEADER_CONTENT_TYPE
} from '../constants/Env'

import { ImageUploader } from 'react-native-image-uploader'

const __url = IMGUR_API_URL_IMAGE

const headers = {
    'content-type': IMGUR_API_HEADER_CONTENT_TYPE,
    'Authorization': IMGUR_API_HEADER_AUTHORIZATION
}

export function uploadImage(base64) {
  let headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'Authorization': IMGUR_API_HEADER_AUTHORIZATION,
  };
  let params = {
    image: base64,
  };

	return ImageUploader.uploadtoServer(__url, headers, params)
		.then((response)  => {
			return (response.data || {})
		})
		.catch((error) => {
			console.error(error)
		})
}
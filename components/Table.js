import React from 'react';
import { View, Text } from 'react-native';

export default class Table extends React.Component {
    renderRow(row, i) {
        console.log(row[this.props.keyIndex])
        return (<View style={{ flex: 1, alignSelf: 'stretch', flexDirection: 'row' }} key={i}>
                {row.map((cell, i) => <View style={{ flex: 1, alignSelf: 'stretch', padding: 5 }} key={i}>
                    <Text style={{color:'#fff',}}>{cell}</Text>
                </View>)}
            </View>);
    }

    render() {
        return (<View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            {this.props.data.map((row, i) => this.renderRow(row, i))}
        </View>);
    }
}
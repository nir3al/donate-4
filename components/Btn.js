import React from 'react';
import {
    Platform,
    Text,
    Button,
    TouchableOpacity
} from 'react-native';

import colors from '../constants/Colors';

export class Btn extends React.Component {
    render() {
        return (<TouchableOpacity
            onPress={this.props.onPress}
            style={[{
                backgroundColor: this.props.bgColor||this.props.color||colors.btnBgColor,
                paddingVertical: 10,
                borderRadius: 6,
                paddingHorizontal: 13,
            }, this.props.bgStyle]}>
            <Text style={[{
                color: this.props.fgColor || colors.btnFgColor,
                fontWeight: "700",
            }, this.props.fgStyle]}>{this.props.title}</Text>
        </TouchableOpacity>)
    }
}
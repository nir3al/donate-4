export const APP_VER = "v1.2"

export const API_URL = "https://donate-api.glitch.me"

export const IMGUR_API_URL = "https://api.imgur.com"
export const IMGUR_API_URL_IMAGE = `${IMGUR_API_URL}/3/image`
export const IMGUR_API_HEADER_AUTHORIZATION = "Client-ID 02f1630d3e3ee6a"
export const IMGUR_API_HEADER_CONTENT_TYPE = "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW"

export const GOOGLE_MAPS_DIRECTIONS_API_KEY = "AIzaSyDNHBNG4CmG_pSgAt1Aa5F0EpBdAJi7OjY"
export const GOOGLE_MAPS_API_URL = "https://maps.googleapis.com/maps/api/place/details/json?key=AIzaSyDioBGXRFyzk2dzZvlpSMIIKBpDGIXWSaA&placeid="
export const DARK_SKY_API_URL = "https://api.darksky.net/forecast/d56f8cb05558b141e61a50d32b3bb8b0/"
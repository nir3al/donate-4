const tintColor = '#B4DC74';

export default {
  tintColor,
  tabIconDefault: '#fff',
  tabIconSelected: tintColor,
  tabBar: '#fefefe',
  errorBackground: 'red',
  errorText: '#fff',
  warningBackground: '#EAEB5E',
  warningText: '#666804',
  noticeBackground: tintColor,
  noticeText: '#fff',

  appBgColor: '#6A4C93', //'#1982C4', 
  appLinesColor: 'rgba(255, 255, 255, 0.06)',
  appHeaderBgColor: '#6A4C93cc',

  appH1Color: '#ffffffdd',
  appH2Color: 'rgba(255, 255, 255, 0.6)',
  appBodyFgColor: '#ffffffbb',
  appBodyFgColor2: '#ffffff99',

  btnBgColor: 'rgba(255, 255, 255, 0.4)',
  btnBgPrimaryColor: '#8AC926bb',
  btnFgColor: '#333',

  verifyBadgeColor: "#8AC926",
};

import React from 'react';
import { Platform, StatusBar, StyleSheet, View, BackHandler } from 'react-native';

import { AppLoading, Asset, Font } from 'expo';
import { Ionicons } from '@expo/vector-icons';

import RootNavigation from './navigation/RootNavigation';

export default class App extends React.Component {
  state = {
    isLoadingComplete: false,
  };

  render() {
    if (!this.state.isLoadingComplete && !this.props.skipLoadingScreen) {
      return (
        <AppLoading
          startAsync={this._loadResourcesAsync}
          onError={this._handleLoadingError}
          onFinish={this._handleFinishLoading}
        />
      );
    } else {
      return (
        <View style={styles.container}>
          {Platform.OS === 'ios' && <StatusBar 
            // barStyle="default" 
            backgroundColor="#333"
            barStyle="dark-content"
            hidden={true} />}
          {Platform.OS === 'android' && <StatusBar 
            style={styles.statusBarUnderlay}
            backgroundColor="#333"
            barStyle="dark-content" 
            hidden={true} />}
          <RootNavigation />
        </View>
      );
    }
  }

  componentWillMount() {
    BackHandler.addEventListener('hardwareBackPress', function() {
      // this.onMainScreen and this.goBack are just examples, you need to use your own implementation here
      // Typically you would use the navigator here to go to the last state.
      if (!this.props || !this.props.navigation) {
        return true;
      }
      if (this.props.navigation.state.routeName==="Login") {
        return true;
      }
      return false;
    });
  }

  _loadResourcesAsync = async () => {
    return Promise.all([
      Asset.loadAsync([
        require('./assets/images/robot-dev.png'),
        require('./assets/images/robot-prod.png'),
      ]),
      Font.loadAsync({
        // This is the font that we are using for our tab bar
        ...Ionicons.font,
        // We include SpaceMono because we use it in HomeScreen.js. Feel free
        // to remove this if you are not using it in your app
        'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf'),
      }),
    ]);
  };

  _handleLoadingError = error => {
    // In this case, you might want to report the error to your error
    // reporting service, for example Sentry
    console.warn(error);
  };

  _handleFinishLoading = () => {
    this.setState({ isLoadingComplete: true });
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#333',
    // marginTop: Expo.Constants.statusBarHeight,
  },
  statusBarUnderlay: {
    height: StatusBar.currentHeight,
    marginTop: -1 * StatusBar.currentHeight,
    backgroundColor: 'rgba(0,0,0,0.2)',
  },
});

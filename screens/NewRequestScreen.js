import React from 'react';
import {
	Platform,
	ScrollView,
	StyleSheet,
	Text,
	View,
	Button,
	StatusBar,
	TextInput,
	Picker,
	Alert,
	Image,
	Switch,
} from 'react-native';
import { ImagePicker, Permissions } from 'expo'

import { uploadImage } from '../api/Images'
import { sendRequest } from '../api/Request'
import { getUser } from '../api/Auth'

import { Btn } from '../components/Btn';
import colors from '../constants/Colors';

export default class NewRequestScreen extends React.Component {

	state = {
		$name: "",
		$comments: "",
		$victimStatus: "safe",
		$numChildrenBelow5: "",
		$numChildrenAbove5: "",
		$numPregnant: "",
		$numDisabled: "",
		$numLadies: "",
		$numMen: "",
		$numSick: "",
		$photoUrl: null,
		$verifCode: Math.floor(Math.random() * 90000) + 10000,
		$phone: '',
		isLocSet: false,
		locShareWrapper: { name: null, },
		isBusy: false,
		$photoData: {},
	};

	static navigationOptions = ({ navigation }) => {
		return {
			headerStyle: { 
				marginTop: -1 * StatusBar.currentHeight,
				backgroundColor: colors.appHeaderBgColor,
			},
			title: "New request",
			headerLeft: null,
		}
	}

	async componentWillMount() {
		const { status } = await Permissions.askAsync(Permissions.CAMERA);
		await Permissions.askAsync(Permissions.CAMERA_ROLL);
		this.setState({ hasCameraPermission: status === 'granted' });
	  }

	render() {
		return (
			<View style={styles.container}>
				<ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
					<View style={styles.formContainer}>

						<View style={styles.formEntry}>
							<Text style={styles.formEntrySubsectionHeader}>What's your name?</Text>
							<Text style={styles.formEntryRequiredNote}>* This is required</Text>
							<TextInput placeholder="My name" style={styles.inputText} onChangeText={(text) => this.setState({ $name: text })} underlineColorAndroid='transparent' placeholderTextColor='#ffffff77' />
						</View>

						<View style={styles.formEntry}>
							<Text style={styles.formEntrySubsectionHeader}>Are you safe? </Text>
							{/* <View style={styles.inputPickerWrapper}>
								<Picker
									style={styles.inputPicker}
									selectedValue={this.state.$victimStatus}
									onValueChange={(itemValue, itemIndex) => this.setState({ $victimStatus: itemValue })}>
									<Picker.Item label="Safe" value="safe" style={styles.inputPickerItem} />
									<Picker.Item label="Not safe" value="not-safe" style={styles.inputPickerItem} />
								</Picker>
							</View> */}
							<View style={styles.rowContainer}>
								<Switch
									onValueChange={() => this.setState(state => ({
										$victimStatus: (state.$victimStatus=='safe')?'unsafe':'safe',
									}))}
									value={this.state.$victimStatus!='safe'}
									onTintColor="red"
									tintColor="#6ef442"
									/>
								<Text style={{color: '#fff', margin: 3, marginLeft: 10, lineHeight: 22, }}>{(this.state.$victimStatus=='safe')?'Safe':'Not safe'}</Text>
							</View>
						</View>

						<View style={styles.formEntry}>
							<Text style={styles.formEntrySubsectionHeader}>How can we contact you?</Text>
							<Text style={styles.formEntryRequiredNote}>* This is required</Text>
							<TextInput placeholder="Contact number" style={styles.inputText} onChangeText={(text) => this.setState({ $phone: text })} underlineColorAndroid='transparent' placeholderTextColor='#ffffff77' />
						</View>

						<View style={styles.formEntry}>
							<Text style={styles.formEntrySubsectionHeader}>How many people are there?</Text>
							<View style={styles.formSubSection}>
								<View style={styles.formEntry}>

									<Text style={styles.formEntryHeader}>Children below 5</Text>
									<TextInput placeholder="0" style={styles.inputText} keyboardType='numeric' value={this.state.$numChildrenBelow5} onChangeText={(text) => this.setState({ $numChildrenBelow5: text })} underlineColorAndroid='transparent' placeholderTextColor='#ffffff77' />

									<Text style={styles.formEntryHeader}>Children above 5</Text>
									<TextInput placeholder="0" style={styles.inputText} keyboardType='numeric' value={this.state.$numChildrenAbove5} onChangeText={(text) => this.setState({ $numChildrenAbove5: text })} underlineColorAndroid='transparent' placeholderTextColor='#ffffff77' />

									<Text style={styles.formEntryHeader}>Pregnant mothers</Text>
									<TextInput placeholder="0" style={styles.inputText} keyboardType='numeric' value={this.state.$numPregnant} onChangeText={(text) => this.setState({ $numPregnant: text })} underlineColorAndroid='transparent' placeholderTextColor='#ffffff77' />

									<Text style={styles.formEntryHeader}>Sick people</Text>
									<TextInput placeholder="0" style={styles.inputText} keyboardType='numeric' value={this.state.$numSick} onChangeText={(text) => this.setState({ $numSick: text })} underlineColorAndroid='transparent' placeholderTextColor='#ffffff77' />

									<Text style={styles.formEntryHeader}>Disabled people</Text>
									<TextInput placeholder="0" style={styles.inputText} keyboardType='numeric' value={this.state.$numDisabled} onChangeText={(text) => this.setState({ $numDisabled: text })} underlineColorAndroid='transparent' placeholderTextColor='#ffffff77' />

									<Text style={styles.formEntryHeader}>Other women</Text>
									<TextInput placeholder="0" style={styles.inputText} keyboardType='numeric' value={this.state.$numLadies} onChangeText={(text) => this.setState({ $numLadies: text })} underlineColorAndroid='transparent' placeholderTextColor='#ffffff77' />

									<Text style={styles.formEntryHeader}>Other men</Text>
									<TextInput placeholder="0" style={styles.inputText} keyboardType='numeric' value={this.state.$numMen} onChangeText={(text) => this.setState({ $numMen: text })} underlineColorAndroid='transparent' placeholderTextColor='#ffffff77' />

								</View>
							</View>
						</View>

						<View style={styles.formEntry}>
							<Text style={styles.formEntrySubsectionHeader}>Any other specific comment?</Text>
							<TextInput placeholder="Comment" multiline={true} style={styles.inputText} onChangeText={(text) => this.setState({ $comments: text })} underlineColorAndroid='transparent' placeholderTextColor='#ffffff77' />
						</View>

						<View style={{flexDirection: 'row', flexWrap: 'wrap',}}>
							<Btn bgStyle={styles.btnListItem} title="Set location" color={colors.btnBgColor} onPress={this._getLocationAsync} />

							<Btn bgStyle={styles.btnListItem} title="Attach photo" color={colors.btnBgColor}  onPress={() => {
								this._captureImage()
							}} />

							<Btn bgStyle={styles.btnListItem} title="Send request" color={colors.btnBgPrimaryColor}  onPress={() => { this._sendRequest(); }} />
						</View>

						<View style={styles.formEndEmptySpace}></View>
					</View>
				</ScrollView>

				{(this.state.isBusy)?<View style={styles.overlay}></View>:null}
			</View>
		);
	}

	_sendRequest() {
		// Alert.alert(
		// 	'Please wait...',
		// 	"Request is being sent",
		// 	[
		// 		{ text: 'OK', onPress: () => null },
		// 	],
		// 	{ cancelable: false }
		// )
		this.setState({isBusy:true})
		sendRequest({

			name: this.state.$name,
			comments: this.state.$comments,
			victimStatus: this.state.$victimStatus,
			photoUrl: this.state.$photoUrl,

			numChildrenBelow5: this.state.$numChildrenBelow5,
			numChildrenAbove5: this.state.$numChildrenAbove5,
			numPregnant: this.state.$numPregnant,
			numDisabled: this.state.$numDisabled,
			numLadies: this.state.$numLadies,
			numMen: this.state.$numMen,
			numSick: this.state.$numSick,

			locName: this.state.locShareWrapper.name,
			locLat: this.state.locShareWrapper.lat,
			locLong: this.state.locShareWrapper.long,
			locPlaceId: this.state.locShareWrapper.placeId,
			locLocationId: this.state.locShareWrapper.locationId,

			userId: __user._id,

			verifCode: this.state.$verifCode,
			phone: this.state.$phone,

			photoData: this.state.$photoData,
		
		})
			.then(savedRef => {
				console.log(this.props.navigation.state.params.user)
				getUser(this.props.navigation.state.params.user._id).then(userObj => {
					this.setState({isBusy:true})
					Alert.alert(
						'Request sent',
						`You will be notified when the request is accepted. Your verification code is "${this.state.$verifCode}".`,
						[
							{ text: 'OK', onPress: () => null },
						],
						{ cancelable: false }
					)
					this.props.navigation.navigate('PendingRequest', { user:userObj, loggedIn: true, refreshFlag: true, verifCode: this.state.$verifCode, })
				})
			}).catch(err => {
				Alert.alert(
					'Request failed',
					"Make sure the data is valid",
					[
						{ text: 'OK', onPress: () => null },
					],
					{ cancelable: false }
				)
			})
	}

	setWrapperState = (obj) => {
		this.setState({ isLocSet: true, })
		this.setState({ locShareWrapper: obj, })
		console.log(this.state.locShareWrapper)
	}

	_getLocationAsync = async () => {

		this.setState({ isLocSet: true, })
		this.setState({ locShareWrapper: { name: null, } })

		this.props.navigation.navigate("LocationPicker2", { loggedIn: true, setWrapperState: this.setWrapperState, })

	}

	_captureImage = async () => {

		const pickerResult = await ImagePicker.launchCameraAsync({
			allowsEditing: false, // higher res on iOS
			aspect: [4, 3],
			base64: true,
		})

		Alert.alert(
			'Uploading photo',
			`Please wait while the photo is uploaded...`,
			[
				{ text: 'OK', onPress: () => null },
			],
			{ cancelable: false }
		)

		if (pickerResult) {
			uploadImage(pickerResult.base64)
				.then(data => {
					if (data) {

						console.log('Uploaded file URL online:', data)
						this.setState({ $photoUrl: data.link, $photoData: data })

						Alert.alert(
							'Photo uploaded successfully',
							'You can now send your request.',
							[
								{ text: 'OK', onPress: () => null },
							],
							{ cancelable: false }
						)

					}
				})
		}

	}

}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#6A4C93',
	},
	contentContainer: {
		padding: 20,
		paddingTop: 10,
		paddingHorizontal: 0,
	},
	btnListItem: {
		marginRight: 10,
		marginBottom: 10,
	},
	formContainer: {
		paddingHorizontal: 20,
		paddingVertical: 10,
	},
	formEntry: {
		paddingBottom: 6,
	},
	formActionAtEnd: {
		paddingVertical: 12,
	},
	formEntryHeader: {
		marginVertical: 5,
    	color: colors.appBodyFgColor2,
	},
	formEntrySubsectionHeader: {
		fontWeight: "500",
		paddingBottom: 5,
		color: colors.appBodyFgColor2,
	},
	inputText: {
		minWidth: 90,
    fontSize: 16,
    paddingVertical: 5,
    lineHeight: 22,
    paddingHorizontal: 10,
    textAlign: 'left',
    color: '#fff',
    backgroundColor: '#ffffff33',
    borderRadius: 4,
	marginHorizontal: 4,
	marginBottom: 10,
	},
	inputPickerWrapper: {
		marginVertical: 5,
		...Platform.select({
			android: {
				borderBottomColor: 'grey',
				borderBottomWidth: 1,
			},
		}),
	},
	inputPicker: {
		height: 26,
	},
	inputPickerItem: {
		textAlign: 'center',
	},
	formSubSection: {
		paddingHorizontal: 20,
	},
	formEntryRequiredNote: {
		color: '#FF595E',
		fontSize: 12,
		fontStyle: 'italic',
		marginBottom: 3,
	},
	formEndEmptySpace: {
		height: 200,
	},
	rowContainer: {
		flexDirection: 'row'
	},
	overlay: {
		flex: 1,
		backgroundColor: 'black',
		position: 'absolute',
		top: 0,
		right: 0,
		bottom: 0,
		left: 0,
		opacity: 0.3
	},
});

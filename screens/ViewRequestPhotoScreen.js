import React from 'react';
import {
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  View,
  Button,
  StatusBar,
  TextInput,
  Picker,
  Alert,
  Image,
} from 'react-native';

import { updateRequest } from '../api/Request'
import { getMatchForFace } from '../api/Faces'

export default class ViewRequestPhotoScreen extends React.Component {

  state = {
    duplicateCount: 0,
  };

  static navigationOptions = ({ navigation }) => {
    return {
      headerStyle: { marginTop: -1 * StatusBar.currentHeight },
      tabBarVisible: true,
      title: "Request - photo",
    }
  }

  async componentDidMount() {
    let reqObj = this.props.navigation.state.params.item;
    reqObj.faceData.forEach(async face => {
      const result = await getMatchForFace(face.faceId, reqObj.locName);
      if (result.isDuplicate) {
        this.setState({duplicateCount:this.state.duplicateCount+1});
      }
    })
  }

  render() {
    console.log(this.props.navigation.state.params.item)
    return (this.props.navigation.state.params.item.photoUrl) ?
        (<View style={styles.container}>
          <Image
            style={styles.image}
            source={{uri: this.props.navigation.state.params.item.photoUrl}}
            resizeMode="contain"
          />
          <View style={styles.overlayContainer}>
            <Text style={styles.overlayText}>{this.props.navigation.state.params.reqPeopleCount} {(this.props.navigation.state.params.reqPeopleCount!=1)?'people are in this request':'person is in this request'}</Text>
            
            <Text style={styles.overlayText}>{this.props.navigation.state.params.item.faceCount} {(this.props.navigation.state.params.item.faceCount!=1)?'people are in this photo':'person is in this photo'}</Text>
            
            {(this.state.duplicateCount>0) ? <Text style={styles.overlayTextRed}>{this.state.duplicateCount} people appear in other requests too</Text> : null}
          </View>
        </View>)
        :
        (<View style={styles.container}>
          <View style={styles.overlayContainer}>
            <Text style={styles.overlayText}>No photo is attached</Text>
          </View>
        </View>)
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  contentContainer: {
    padding: 20,
    paddingTop: 10,
    paddingHorizontal: 0,
  },
  image: {
    flex: 1,
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
  },
  overlayContainer: {
    flex: 1,
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    padding: 20,
  },
  overlayText: {
    backgroundColor: '#333333',
    color: '#ffffff',
    textAlign: 'center',
    padding: 5,
  },
  overlayTextRed: {
    backgroundColor: '#dd0033',
    color: '#ffffff',
    textAlign: 'center',
    padding: 5,
  },
});

import React from 'react';
import {
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  View,
  Button,
  StatusBar,
  TextInput,
  Picker,
  Alert,
} from 'react-native';
import moment from 'moment';
import { Calendar, } from 'react-native-calendars';
import ModalPicker from 'react-native-modal-selector'

const _format = 'YYYY-MM-DD'
const _today =  moment().format(_format)
const _minDate = moment().subtract(1, 'days').format(_format)
const _maxDate = moment().add(15, 'days').format(_format)

const TYPES_COUNT = 5;

import { addDonationInBulk } from '../api/Donation'

import { Btn } from '../components/Btn';
import colors from '../constants/Colors';

export default class DonateScreen extends React.Component {

  initialState = {
      [_today]: {disabled: false,}
  }

  state = {
    _markedDates: this.initialState,
    isBusy: false,
    name: '',
    phone: '',
    address: '',
    type: '',
    category: '',
    amount: '',
    units: 'x 1',
    dates: [],
    all: { food:[], water:[], medicine:[], clothes:[], other:[] }
  }
  
  static navigationOptions = ({navigation}) => {
    return {
      headerStyle: { 
				marginTop: -1 * StatusBar.currentHeight,
				backgroundColor: colors.appHeaderBgColor,
			},
			titleStyle: {
				color: '#ffffffcc'
			},
			tintColor: '#ffffffcc',
      title: "Donate",
      //headerRight: (
      //  <View style={{paddingRight: 15}}>
      //    <Button 
      //      title="Send" 
      //      color="#841584"
      //      onPress={() => null}
      //    />
      //  </View>
      //)
    }
  }

  _onDaySelect = (day) => {
    const _selectedDay = day.dateString;

    let markedDates = {};
    markedDates[_selectedDay] = { selected: true, marked: true, dotColor: "#333399", };
    
    // let marked = true;
    // let markedDates = {}
    // if (this.state._markedDates[_selectedDay]) {
    //   // Already in marked dates, so reverse current marked state
    //   marked = !this.state._markedDates[_selectedDay].marked;
    //   markedDates = this.state._markedDates[_selectedDay];
    // }
    
    // markedDates = {...markedDates, ...{ marked }};
    
    // // Create a new object using object property spread since it should be immutable
    // // Reading: https://davidwalsh.name/merge-objects
    // const updatedMarkedDates = {...this.state._markedDates, ...{ [_selectedDay]: markedDates } }
    
    // // Triggers component to render again, picking up the new state
    this.setState({ _markedDates: markedDates, dates: markedDates, });
  }

  _typesList = [ ['food','Food'], ['water','Water'], ['medicine','Medicine'], ['clothes','Clothes'], ['other','Other'] ]

  _getCategoryItemsMap(donationType) {
    let items = [];
    
    if (donationType=="water") {
      items = [["Small bottles","Small bottles"], ["Large bottles","Large bottles"]];
    } else if (donationType=="food") {
      items = [["Rice and curry","Rice and curry"], ["Fried rice","Fried rice"], ["Milk powder","Milk powder"], ["Biscuits","Biscuits"], ["Bread","Bread"], ['Rice', 'Rice'], ['Dahl', 'Dahl'], ['Sugar', 'Sugar'], ['Soya meat', 'Soya meat'], ['Wheat flour', 'Wheat flour'], ['Tea bags', 'Tea bags'], ['Noodles', 'Noodles']];
    } else if (donationType=="medicine") {
      items = [["Paracetamol","Paracetamol"], ["Anti-inflammatory painkillers","Anti-inflammatory painkillers"], ["Antihistamines","Antihistamines"], ['Soap', 'Soap'], ['Plasters', 'Plasters'], ['Sanitary napkins', 'Sanitary napkins'], ['Mosquito repellent','Mosquito repellent']];
    } else if (donationType=='clothes') {
      items = [["T- Shirts","T- Shirts"], ["Shorts","Shorts"], ["Skirts","Skirts"], ['Blouses','Blouses'], ['Towels','Towels'],['Blankets','Blankets']];
    } else {
      items = [['Mop','Mop'],['Buckets','Buckets'], ['Gloves','Gloves'], ['Slippers','Slippers'], ['Garbage Bags','Garbage Bags'], ['Candles & Matches','Candles & Matches'], ['Masks','Masks'], ['Tents','Tents']]
    }

    return items.map(x => {return {key:x[0],label:x[1]}});
  }

  _getUnitItemsMap(donationType, category, listItem) {
    let items = [];

    if (donationType=="food") {
      items = [["kilograms","kilograms"],["packets","packets"]];
    } else if (donationType=="water") {
      items = [["litres","litres"]];
    } else if (category=="medicine") {
      items = [["packs","packs"]];
    } else if (category=="clothes") {
      items = [["x 1","x 1"]];
    } else {
      items = [["x 1","x 1"]];
    }

    return items.map(x => {return {key:x[0],label:x[1]}});
  }

  _addAndCreateEmpty(type) {
    //// { cat, amount, unit }

    this.setState({ type });

    if (this._isInputValid()) {

      const item = { category:this.state.category, amount:this.state.amount, units:this.state.units };
      let all = Object.assign({}, this.state.all);
      all[type].push(item);
      this.setState({ all });

      this.setState({ category:'', amount:'', units:'x 1' });

    }
  }

  _isInputValid() {
    return this.state.category && this.state.amount && this.state.units;
  }

  _submitDonation() {
    if (this.state.all.food.length>0 || this.state.all.water.length>0 || this.state.all.medicine.length>0 || this.state.all.clothes.length>0 || this.state.all.other.length>0) {
      
      this.setState({isBusy:true})

      let donations = [];
      donations.push(...this.state.all.food);
      donations.push(...this.state.all.water);
      donations.push(...this.state.all.medicine);
      donations.push(...this.state.all.clothes);
      donations.push(...this.state.all.other);

      console.log('this.state.all.water', this.state.all.food);
      console.log('donations', donations);

      const apiItems = donations.map(d => {return {
        name: this.state.name,
        phone: this.state.phone,
        address: this.state.address,
        type: '(part of a list)',
        category: d.category,
        amount: d.amount,
        units: d.units,
        dates: this.state.dates
      };});

      addDonationInBulk(apiItems).then(()=>{
        this.setState({isBusy:false});
        this.props.navigation.navigate("DonationList")
      });

    }
  }

  $inputItem() {
    return (<View style={styles.rowInputs}>
      <ModalPicker
          data={this._getCategoryItemsMap(this.state.type)}
          initValue="Category ?"
          onChange={ category => this.setState({category:category.key}) } 
          style={{
            backgroundColor: '#ffffff33',
          }}
        >
      </ModalPicker>
      <TextInput 
        placeholder="Amount ?" 
        style={styles.rowInputs_TextInput} 
        keyboardType='numeric' 
        onChangeText={(amount) => this.setState({amount})} 
        value={this.state.amount} 
        underlineColorAndroid='transparent' placeholderTextColor='#ffffff77'
      />
      <ModalPicker
          // style={styles.rowInputs_Dropdown}
          data={this._getUnitItemsMap(this.state.type, this.state.category)}
          initValue="Unit ?"
          value={this.state.units}
          onChange={(units)=>{ this.setState({units:units.key}) }} 
          style={[{
            backgroundColor: '#ffffff33',
          }, styles.rowInputs_Dropdown]}
        />
    </View>);
  }

  $btnAdd(type) {
    const text = (this.state.type!=type) ? 'Add entry' : 'Update';
    return (<Btn style={styles.btnNewItem} title={text} onPress={ () => this._addAndCreateEmpty(type) } color={colors.btnBgColor} fgColor={colors.btnFgColor} />);
  }

  $listItems(type) {
    return (<View>
      {this.state.all[type].map((item, i) => (<Text key={i} style={styles.listItemText}>
        {item.category} — {item.amount} {(item.units && item.units!='x 1') ? `in ${item.units}` : ''}
      </Text>))}
    </View>);
  }

  render() {

    let customDatesStyles = [];
    let startDate = new Date();
    let dates = [
      startDate,
      startDate.setDate(startDate.getDate() + 1),
      startDate.setDate(startDate.getDate() + 2),
      startDate.setDate(startDate.getDate() + 3),
      startDate.setDate(startDate.getDate() + 4),
      startDate.setDate(startDate.getDate() + 5),
    ];
    for (let i=0; i<6; i++) {
      customDatesStyles.push({
          startDate: dates[i], // Single date since no endDate provided
          // dateNameStyle: this.styles.someDateNameStyle,
          // dateNumberStyle: this.styles.someDateNumberStyle,
          dateContainerStyle: {backgroundColor: '#'+('#00000'+(Math.random()*(1<<24)|0).toString(16)).slice(-6)},
      });
    }

    return (
      <View style={styles.container}>
        <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
          <View style={styles.formContainer}>

            <View style={styles.formEntry}>
              <Text style={styles.formEntryRequiredNote}>* All fields are required</Text>
            </View>

            <View style={styles.formEntry}>
              <Text style={styles.formEntryHeader}>Name</Text>
              <TextInput placeholder="Name" style={styles.inputText} onChangeText={(name) => this.setState({name})} value={this.state.name} underlineColorAndroid='transparent' placeholderTextColor='#ffffff77' />
            </View>

            <View style={styles.formEntry}>
              <Text style={styles.formEntryHeader}>Contact number</Text>
              <TextInput placeholder="Mobile or landline" style={styles.inputText} onChangeText={(phone) => this.setState({phone})} value={this.state.phone} underlineColorAndroid='transparent' placeholderTextColor='#ffffff77' />
            </View>

            <View style={styles.formEntry}>
              <Text style={styles.formEntryHeader}>Address</Text>
              <TextInput placeholder="Address" multiline={true} style={styles.inputText} onChangeText={(address) => this.setState({address})} value={this.state.address} underlineColorAndroid='transparent' placeholderTextColor='#ffffff77' />
            </View>

            <View style={{marginBottom: 20,}}></View>

            {this._typesList.map((typeKVP, i) => (<View style={styles.formEntry} key={i}>
              <Text style={styles.formEntryHeader}>{typeKVP[1]}</Text>
              <View>
                { this.$listItems(typeKVP[0]) }
                { (this.state.type==typeKVP[0]) ? this.$inputItem() : null }
                <View style={{flexDirection: 'row',}}>{ this.$btnAdd(typeKVP[0]) }</View>
              </View>
            </View>))}

            <View style={styles.formEntry}>
              <Text style={styles.formEntryHeader}>Date</Text>
              {/* TODO: A month calander with day highlighting  */}
              {/* <CalendarStrip
              /> */}
              {/* customDatesStyles={customDatesStyles} */}
              <Calendar
                // Initially visible month. Default = Date()
                current={_today}
                // Minimum date that can be selected, dates before minDate will be grayed out. Default = undefined
                //minDate={'2018-01-23'}
                // Maximum date that can be selected, dates after maxDate will be grayed out. Default = undefined
                //maxDate={'2018-05-23'}
                // Handler which gets executed on day press. Default = undefined
                //onDayPress={(day) => {console.log('selected day', day)}}
                // Month format in calendar title. Formatting values: http://arshaw.com/xdate/#Formatting
                monthFormat={'yyyy MM'}
                // Handler which gets executed when visible month changes in calendar. Default = undefined
                onMonthChange={(month) => {console.log('month changed', month)}}
                // Hide month navigation arrows. Default = false
                hideArrows={false}
                // Replace default arrows with custom ones (direction can be 'left' or 'right')
                // renderArrow={(direction) => (<Arrow />)}
                // Do not show days of other months in month page. Default = false
                hideExtraDays={true}
                // If hideArrows=false and hideExtraDays=false do not switch month when tapping on greyed out
                // day from another month that is visible in calendar page. Default = false
                disableMonthChange={true}
                // If firstDay=1 week starts from Monday. Note that dayNames and dayNamesShort should still start from Sunday.
                firstDay={1}
                // Hide day names. Default = false
                hideDayNames={true}
                // Show week numbers to the left. Default = false
                showWeekNumbers={true}
                theme={{
                  selectedDayBackgroundColor: '#8AC926',
                  selectedDayTextColor: '#333',
                  backgroundColor: 'transparent',
                  calendarBackground: 'transparent',
                  textSectionTitleColor: '#ffffff',
                  todayTextColor: '#00adf5',
                  dayTextColor: '#ffffffaa',
                  textDisabledColor: '#ffffff22',
                  monthTextColor: '#fff',
                }}

                selected={ this.state.marked }

                minDate={_minDate}
                maxDate={_maxDate}

                // hideArrows={true}

                onDayPress={(day) => this._onDaySelect(day)}
                markedDates={this.state._markedDates}
              />
            </View>

            <View style={[styles.formActionAtEnd, {flexDirection: 'row',}]}>
              <Btn title="Submit" color={colors.btnBgPrimaryColor} fgColor={colors.btnFgColor} onPress={() => this._submitDonation()} />
            </View>

            <View style={styles.formEndEmptySpace}></View>
          </View>
        </ScrollView>

        {(this.state.isBusy)?<View style={styles.overlay}></View>:null}
      </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.appBgColor,
  },
  contentContainer: {
    padding: 20,
    paddingTop: 10,
    paddingHorizontal: 0,
  },
  listItemText: {
    fontStyle: 'italic',
    marginBottom: 5,
    color: '#ccc',
  },
  rowInputs: {
    flexDirection: 'row',
    flex: 1,
    marginBottom: 4,
  },
  rowInputs_TextInput: {
    minWidth: 90,
    fontSize: 16,
    paddingVertical: 5,
    lineHeight: 22,
    paddingHorizontal: 10,
    textAlign: 'right',

    // padding: 5,
    lineHeight: 22,
    // minWidth: 150,
    textAlign: 'center',
    color: '#fff',
    backgroundColor: '#ffffff33',
    borderRadius: 4,
    // margin: 3,
    marginHorizontal: 4,
    borderWidth: 1,
    borderColor: '#ffffff99',
    // maxWidth: 100,
  },
  rowInputs_Dropdown: {
    // maxWidth: 100,
    // width: 100,
  },
  btnNewItem: {
  },
  formContainer: {
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  formEntry: {
    paddingBottom: 6,
  },
  formActionAtEnd: {
    paddingVertical: 12,
  },
  formEntryHeader: {
    paddingBottom: 5,
    color: '#ffffff',
  },
  formEntrySubsectionHeader: {
    fontWeight: "500",
    paddingBottom: 5,
  },
  inputText: {
    minWidth: 90,
    fontSize: 16,
    paddingVertical: 5,
    lineHeight: 22,
    paddingHorizontal: 10,
    textAlign: 'left',
    color: '#fff',
    backgroundColor: '#ffffff33',
    borderRadius: 4,
    marginHorizontal: 4,
  },
  inputPickerWrapper: {
    marginVertical: 5,
    ...Platform.select({
      android: {
        // borderBottomColor: 'grey',
        // borderBottomWidth: 1,
      },
    }),
  },
  inputPicker: {
    height: 26,
  },
  inputPickerItem: {
    textAlign: 'center',
  },
  formSubSection: {
    paddingHorizontal: 20,
  },
  formEntryRequiredNote: {
    color: '#FF595E',
    fontSize: 12,
    fontStyle: 'italic',
  },
  formEndEmptySpace: {
    height: 200,
  },
  overlay: {
		flex: 1,
		backgroundColor: 'black',
		position: 'absolute',
		top: 0,
		right: 0,
		bottom: 0,
		left: 0,
		opacity: 0.3
	},
});

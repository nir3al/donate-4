import React from 'react';
import {
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  View,
  Button,
  StatusBar,
  TextInput,
  Picker,
  Alert,
  Image,
  RefreshControl,
} from 'react-native';

import { updateRequest, finishRequest, getRequest } from '../api/Request'
import { getUser } from '../api/Auth'

import {Btn} from '../components/Btn';
import colors from '../constants/Colors';

export default class PendingRequestScreen extends React.Component {

  state = {
    refreshing: false,
    isBusy: false,
  }

  static navigationOptions = ({ navigation }) => {
    return {
      headerStyle: { 
				marginTop: -1 * StatusBar.currentHeight,
				backgroundColor: colors.appHeaderBgColor,
			},
      tabBarVisible: true,
      title: "Your request status",
      headerLeft: null,
    }
  }

  componentDidMount() {
		this._sub = this.props.navigation.addListener(
			'didFocus',
			this._onPageFocus_Reload
    )
    getRequest(this.props.navigation.state.params.user.lastRequest._id).then(req => {
      this.setState({request:req})
    })
    this._onPageFocus_Reload()
	}

  render() {
    return (
      <View style={styles.container}>
        <ScrollView 
          style={styles.container} 
          contentContainerStyle={styles.contentContainer}
          refreshControl={
						<RefreshControl
						  refreshing={this.state.refreshing}
						  onRefresh={this._onRefresh.bind(this)}
						/>
					}
          >
        {(!this.state.request || !this.state.request.accepted) ?
          (<View>
            <Text style={styles.centeredText}>Request is pending aproval.</Text>
            <Text style={styles.centeredText}>Your reference code is {this.props.navigation.state.params.user.lastRequest.verifCode}</Text>
          </View>)
          :
          (<View>
            <Text style={styles.centeredText}>Request has been approved.</Text>
            <Text style={styles.centeredText}>Your reference code is {this.props.navigation.state.params.user.lastRequest.verifCode}</Text>
            <View style={{
              marginTop: 55, 
              width: '100%',
              height: '30%',
              justifyContent: 'center',
              alignItems: 'center',
              flexDirection: 'row'
            }}>
              <Btn title="Confirm donation was received" bgColor={colors.btnBgColor} fgColor={colors.btnFgColor} onPress={() => this._onDonationReceived()} />
            </View>
          </View>)}
        </ScrollView>

        {(this.state.isBusy)?<View style={styles.overlay}></View>:null}
      </View>
    );
  }

  _onDonationReceived() {
    this.setState({isBusy:true})
    getUser(this.props.navigation.state.params.user._id).then(user=> {
      console.log(user)
      finishRequest(user).then(() => {
        this.setState({isBusy:false})
        user.lastRequest = null
        this.props.navigation.navigate('NewRequest', {user})
      })
    })
  }

  _onRefresh() {
		this.setState({refreshing: true});
		this._onPageFocus_Reload().then(() => {
		  this.setState({refreshing: false});
		});
  }

  _onPageFocus_Reload = () => {
		return getUser(this.props.navigation.state.params.user._id)
			.then(_user => {
				this.setState({user: _user})
			})
	}

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.appBgColor,
  },
  contentContainer: {
    padding: 20,
    paddingTop: 10,
    paddingHorizontal: 0,
  },
  centeredText: {
    textAlign: 'center',
    color: '#fff',
  },
  overlay: {
		flex: 1,
		backgroundColor: 'black',
		position: 'absolute',
		top: 0,
		right: 0,
		bottom: 0,
		left: 0,
		opacity: 0.3
	},
});

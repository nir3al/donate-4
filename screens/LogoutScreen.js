import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
} from 'react-native';

import { MonoText } from '../components/StyledText';

export default class LogoutScreen extends React.Component {
  static navigationOptions = {
    header: null,
    tabBarVisible: false,
    title: "Log out",
  };

  state = {
    email: null,
    pass: null,
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
          <View style={styles.formContainer}>
            <Image
              style={{width: 150, height: 63}}
              source={require('../assets/images/logo.png')}
            />
            <Text style={styles.formHeader}>Logged out</Text>
            <View style={{margin:9}} />
            <Text>
              Log back in.
            </Text>
            <View style={{margin:12}} />
            {/* <TextInput value={this.state.email} onChangeText={(email)=>this.setState({email})} placeholder='E-mail' style={styles.loginInput} />
            <TextInput value={this.state.pass} onChangeText={(pass)=>this.setState({pass})} placeholder='Password' secureTextEntry={true} style={styles.loginInput} />
            <View style={{margin:9}} /> */}
            <Button 
                onPress={(x)=>{
                  this.props.navigation.navigate("Login")
                }}
                title="Log in"
                color="#841584"
            />
            {/* <View style={{margin:12}} />
            <Text>
              If you don't already have an account, create one.
            </Text>
            <View style={{margin:9}} />
            <Button 
                onPress={(x)=>{this.props.navigation.navigate("Signup")}}
                title="Sign up"
                color="#555"
            /> */}
          </View>
        </ScrollView>
        <View style={styles.tabBarInfoContainer}>
           <Text style={{paddingHorizontal: 10,}}>Contact us if you have any problems.</Text>

          <View>
            <MonoText style={styles.codeHighlightText}>support@donate.lk</MonoText>
          </View> 
        </View>
      </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  contentContainer: {
    padding: 20,
    paddingTop: 30,
  },
  formContainer: {
    paddingVertical: 20,
    alignItems: 'center',
  },
  formHeader: {
    fontSize: 24,
  },
  loginInput: {
    fontSize: 16,
    padding: 5,
    lineHeight: 22,
    minWidth: 150,
    textAlign: 'center',
  },
  codeHighlightText: {
    color: 'rgba(96,100,109, 0.8)',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
  },
});

import React from 'react';
import {
    Platform,
    ScrollView,
    StyleSheet,
    Text,
    View,
    Button,
    StatusBar,
    TextInput,
    Picker,
    Alert,
    Image,
    Dimensions
} from 'react-native';

import { updateRequest } from '../api/Request'
import { getItems, updateStock, checkStock } from '../api/Stock'
import PureChart from 'react-native-pure-chart';

import Table from '../components/Table'
import { Btn } from '../components/Btn'

import colors from '../constants/Colors'

// import PureChart from 'react-native-pure-chart';
// import { StackedBarChart } from 'react-native-svg-charts'
// import * as shape from 'd3-shape'
const { height, width } = Dimensions.get('window')
export default class AcceptRequestScreen extends React.Component {

    state = {
        barData: null,
        canAccept: false,
        request: null,
        items: null,
        isAccepted: false,
        itemsList: null,
        graphUrl: 'http://chart.googleapis.com/chart?cht=bvs&chs=370x360&chd=t:0,0,0,0,0,0,0|0,0,0,0,0,0,0|0,0,0,0,0,0,0|0,0,0,0,0,0,0|0,0,0,0,0,0,0|0,0,0,0,0,0,0|0,0,0,0,0,0,0&chbh=20,30&chxs=0,FFFFFF,13,0,t|1,FFFFFF,13,0,t&chf=bg,s,6A4C93&chxt=x,y&chxl=|0:||||||Yesterday|Today&chco=4D89F9,C6D9FD,ff9900,4D89F9,C6D9FD,ff9900,C6D9FD&chds=a',
    };

    static navigationOptions = ({ navigation }) => {
        // REQUIRES navigation state:
        //  request object - OK, isAccepted boolean - OK
        return {
            headerStyle: {
                marginTop: -1 * StatusBar.currentHeight,
                backgroundColor: colors.appBgColor,
            },
            tabBarVisible: true,
            title: "Items to transport",
        }
    }

    _getUnit(category) {
        let items;

        if ([["Small bottles", "Small bottles"], ["Large bottles", "Large bottles"]].some(x => x[0] === category)) {
            items = [["litres", "litres"]];
        } else if ([["Rice and curry", "Rice and curry"], ["Fried rice", "Fried rice"], ["Milk powder", "Milk powder"], ["Bread", "Bread"], ['Rice', 'Rice'], ['Dahl', 'Dahl'], ['Sugar', 'Sugar'], ['Soya meat', 'Soya meat'], ['Wheat flour', 'Wheat flour']].some(x => x[0] == category)) {
            items = [["kilograms", "kilograms"]];
        } else if ([["Biscuits", "Biscuits"], ['Tea bags', 'Tea bags'], ['Noodles', 'Noodles']].some(x => x[0] === category)) {
            items = [["packets", "packets"]];
        } else if ([["Paracetamol", "Paracetamol"], ["Anti-inflammatory painkillers", "Anti-inflammatory painkillers"], ["Antihistamines", "Antihistamines"], ['Soap', 'Soap'], ['Plasters', 'Plasters'], ['Sanitary napkins', 'Sanitary napkins'], ['Mosquito repellent', 'Mosquito repellent']].some(x => x[0] == category)) {
            items = [["packs", "packs"]];
        } else if ([["T- Shirts", "T- Shirts"], ["Shorts", "Shorts"], ["Skirts", "Skirts"], ['Blouses', 'Blouses'], ['Towels', 'Towels'], ['Blankets', 'Blankets']].some(x => x[0] == category)) {
            items = [["", ""]];
        } else {
            items = [["", ""]];
        }

        return items[0][0]
    }

    encodeTable = (data) => data.map(arr => arr.reduce((s, x) => `${s},${x}`)).reduce((s, x) => `${s}|${x}`);

    setGraphUrl(data, nameList) {
        let url = 'http://chart.googleapis.com/chart?cht=bvs&chs=420x360&chd=t:' // vv
            + data // ^^
            + '&chbh=20,30' // bar spacing (bar size, space)
            + '&chxt=x,y&chxl=|0:' // vv
            + nameList // ^^
            + '&chf=bg,s,6A4C93' // bg color
            + '&chxs=0,FFFFFF,13,0,t|1,FFFFFF,13,0,t' // axis label colors
            // +'&chdl=Water|Food|Snacks|Medicine|Clothes|Utilities'
            + '&chdlp=t'
            + '&chdls=FFFFFF,14'
            + '&chco=4D89F9,C6D9FD,FFED00,00B500,FF00AB,00EDFF' // ^^ // bar colors
            + '&chds=a';

        this.setState({ graphUrl: url });
    }

    setGraphUrl2(data) {
        let url = 'http://chart.googleapis.com/chart?cht=bvs&chs=500x360&chd=t:' // vv
        +this.encodeTable(data) // ^^
        +'&chbh=20,30' // bar spacing (bar size, space)
        +'&chxt=x,y&chxl=|0:' // vv
        // +'||||||Yesterday|Today' // ^^
        + Object.keys(this.state.result.items).reduce((str, x)=>str+'|'+x.split(' ').join('-'), '').substr(1)
        +'&chf=bg,s,6A4C93' // bg color
        +'&chxs=0,FFFFFF,13,0,t|1,FFFFFF,13,0,t' // axis label colors
        // +'&chdl=Water|Food|Snacks|Medicine|Clothes|Utilities'
        +'&chdlp=t'
        +'&chdls=FFFFFF,14'
        +'&chco=4D89F9,C6D9FD,FFED00,00B500,FF00AB,00EDFF' // ^^ // bar colors
        +'&chds=a';

        console.log(Object.keys(this.state.result.items).reduce((str, x)=>str+'|'+x.split(' ').join('-'), '').substr(1));

        this.setState({graphUrl:url});
    }

    flipMatrix = matrix => (
        matrix[0].map((column, index) => (
            matrix.map(row => row[index])
        ))
    );
    
    getTableData(todaysCounts) {

        let max = todaysCounts.reduce((max, x) => (max < x) ? x : max, 0);
        todaysCounts = todaysCounts.map(x => (x >= max / 20) ? x / 100 : x);

        let weeklyStock = [0, 1, 2, 3, 4, 5, 6].map(i => todaysCounts.map(x => x * (i + 4 + Math.random() * 4) / 4));
        weeklyStock = this.flipMatrix(weeklyStock.reverse());

        console.log('weeklyStock (reversed)', weeklyStock);
        return weeklyStock;
    }

    getTableData2(todaysCounts) {
        let weeklyStock = [0].map(i => todaysCounts.map(x=>x));
        // weeklyStock = this.flipMatrix(weeklyStock.reverse());
        weeklyStock = weeklyStock.reverse();

        console.log('weeklyStock 2 (reversed)', weeklyStock);
        return weeklyStock;
    }

    async componentWillMount() {
        const request = this.props.navigation.state.params.request
        const isAccepted = this.props.navigation.state.params.isAccepted
        this.setState({ request, isAccepted, barData: null })

        console.log(request, isAccepted)

        getItems(request._id).then(items => {
            this.setState({ items })

            checkStock(request._id).then(result => {
                console.log(result)
                this.setState({ canAccept: result.verdict, items: result.items, })

                let itemsList = Object.keys(result.items).map((key) => [result.items[key], this._getUnit(key), key])
                itemsList.forEach(item => {
                    if (item[0]) {
                        const i = item[0].toString().indexOf('.');
                        if (i > - 1) {
                            item[0] = item[0].toString().substring(0, i + 2);
                        }
                    }
                })
                this.setState({ itemsList, })

                let _obj = {
                    water: 0,
                    food: 0,
                    snacks: 0,
                    medicine: 0,
                    clothes: 0,
                    utilities: 0,
                };

                // let obj = result.stock;
                let stockKeys = Object.keys(result.stock).filter(x => x!='__name__'&&x!='_id');

                stockKeys.forEach(k => result.stock[k] = +result.stock[k] - +result.items[k]);
                let obj = result.stock;
                let my_obj = new Array();
                let my_names = new Array();
                let nameList = '';

                Object.keys(obj).filter(x => x != '__name__' && x != '_id').forEach((y) => {
                    if ([["Small bottles", "Small bottles"], ["Large bottles", "Large bottles"]].some(x => x[0] == y)) {
                        _obj.water += obj[y]
                    } else if ([["Rice and curry", "Rice and curry"], ["Fried rice", "Fried rice"], ["Milk powder", "Milk powder"], ["Bread", "Bread"], ['Rice', 'Rice'], ['Dahl', 'Dahl'], ['Sugar', 'Sugar'], ['Soya meat', 'Soya meat'], ['Wheat flour', 'Wheat flour']].some(x => x[0] == y)) {
                        _obj.food += obj[y];
                    } else if ([["Biscuits", "Biscuits"], ['Tea bags', 'Tea bags'], ['Noodles', 'Noodles']].some(x => x[0] == y)) {
                        _obj.snacks += obj[y];
                    } else if ([["Paracetamol", "Paracetamol"], ["Anti-inflammatory painkillers", "Anti-inflammatory painkillers"], ["Antihistamines", "Antihistamines"], ['Soap', 'Soap'], ['Plasters', 'Plasters'], ['Sanitary napkins', 'Sanitary napkins'], ['Mosquito repellent', 'Mosquito repellent']].some(x => x[0] == y)) {
                        _obj.medicine += obj[y];
                    } else if ([["T- Shirts", "T- Shirts"], ["Shorts", "Shorts"], ["Skirts", "Skirts"], ['Blouses', 'Blouses'], ['Towels', 'Towels'], ['Blankets', 'Blankets']].some(x => x[0] == y)) {
                        _obj.clothes += obj[y];
                    } else {
                        _obj.utilities += obj[y];
                    }

                    my_obj.push({ x: y, y: obj[y] });
                    this.setState({ barData: my_obj })
                    console.log('my names sweety', my_obj);
                });

                // let stocks = Object.keys(_obj).map((key) => _obj[key]);
                // console.log('stocks', stocks);
                // this.setGraphUrl(this.getTableData(stocks));
                // this.setGraphUrl(my_obj, my_names);
            })
        })
    }

    render() {
        let sampleData = [
            {
                seriesName: 'series1',
                data: this.state.barData,
                color: '#297AB1'
            },
        ]
        return (
            <View style={styles.container}>
                <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
                    <View style={{ flex: 0, alignItems: "center" }}>
                        <Text style={{ fontSize: 20, color: "#FFFFFF", fontWeight: "bold", marginBottom: 10 }}>Total Stock</Text>
                    </View>
                    {/* <StackedBarChart
                    style={ { height: 200, } }
                    data={ this.state.graphData }
                    colors={[ '#8800cc', '#aa00ff', '#cc66ff', '#eeccff', '#ff8800' ]}
                    showGrid={ false }
                    contentInset={ { top: 30, bottom: 30 } }
                /> */}
                    <ScrollView horizontal={true} style={{ marginBottom: 10, paddingBottom: 10, }}>
                        {/* <Image source={{uri:this.state.graphUrl}} style={{width:420, height:360}} /> */}
                        {this.state.barData != null ? <PureChart backgroundColor={"rgba(0, 0, 0, 0.5)"} height={height * 0.5} data={sampleData} type='bar' /> : <Text>Loading...</Text>}
                    </ScrollView>
                    {/* <PureChart data={this.state.sampleData} type='bar' /> */}
                    {/* {this.state.itemsList.map(item => <Text key={item.category}>{item.amount} {item.units} of {item.category}</Text>)} */}
                    <View style={{ flex: 0, alignItems: "center" }}>
                        <Text style={{ fontSize: 20, color: "#FFFFFF", fontWeight: "bold", marginVertical: 15 }}>Predicted amount of needs</Text>
                    </View>
                    {(this.state.itemsList) ? <Table data={this.state.itemsList} /> : null}
                    {this.btnAcceptReq()}
                    {(!this.state.canAccept) ? <Text style={{ color: '#FF595E' }}>This request can't be accepted because the stocks are not enough.</Text> : null}
                </ScrollView>
            </View>
        );
    }

    btnAcceptReq() {
        if (!this.state.isAccepted) {
            return (
                <View style={{
                    flexDirection: 'row',
                }}>
                    <Btn
                        title="Accept request"
                        bgColor={colors.btnBgPrimaryColor}
                        fgColor={colors.btnFgColor}
                        bgStyle={{
                            marginTop: 20,
                            marginBottom: 100,
                        }}
                        disabled={!this.state.canAccept}
                        onPress={() => {
                            this._acceptRequest();
                            this.setState({ isAccepted: true, });
                            this.props.navigation.navigate("RequestList", { loggedIn: true, });
                        }} />
                </View>
            );
        } else {
            return null;
        }
    }

    _acceptRequest() {
        let _item = Object.assign({}, this.state.request)
        _item.accepted = true
        _item.acceptedBy = __user._id
        updateRequest(_item)

        Alert.alert(
            "Request accepted",
            'Thank you!',
            [
                { text: 'OK', onPress: () => null },
            ],
            { cancelable: false, }
        )

        this.props.navigation.navigate('RequestList', { loggedIn: true, });
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.appBgColor,
    },
    contentContainer: {
        padding: 20,
        paddingTop: 10,
        paddingHorizontal: 0,
        margin: 20,
        marginTop: 10,
    },
});

/*

var encodeTable = (data) => data.map(arr => arr.reduce((s,x) => `${s},${x}`)).reduce((s,x) => `${s}|${x}`)

function setGraphUrl(data) {

let nums = encodeTable(data);

        let url = 'http://chart.googleapis.com/chart?cht=bvs&chs=370x360&chd=t:' // vv

        +nums        
        //+'5,9,2,20,20,20,20|10,16,5,20,20,20,20|19,5,15,20,20,20,20|10,16,5,20,20,20,20|19,5,15,20,20,20,20|10,16,5,20,20,20,20|19,5,15,20,20,20,20' // ^^
        +'&chbh=20,30' // bar spacing (bar size, space)
        +'&chxt=x,y&chxl=|0:' // vv
        +'||||||Yesterday|Today' // ^^
        +'&chf=bg,s,6A4C93' // bg color
        +'&chxs=0,FFFFFF,13,0,t|1,FFFFFF,13,0,t' // axis label colors
        +'&chco=' // vv
        +'4D89F9,C6D9FD,ff9900,4D89F9,C6D9FD,ff9900,C6D9FD' // ^^ // bar colors
        +'&chds=a';

        return url;
    }
setGraphUrl([[5,9,2,20,20,20,20],[10,16,5,20,20,20,20],[19,5,15,20,20,20,20],[10,16,5,20,20,20,20],[19,5,15,20,20,20,20],[10,16,5,20,20,20,20],[19,5,15,20,20,20,20]])

*/
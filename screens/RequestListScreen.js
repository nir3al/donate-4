import React from 'react';
import {
	Image,
	Platform,
	ScrollView,
	StyleSheet,
	Text,
	View,
	FlatList,
	SectionList,
	Button,
	StatusBar,
	RefreshControl,
} from 'react-native';

import { Ionicons } from '@expo/vector-icons';
import { getAllRequests } from '../api/Request';

import { Btn } from '../components/Btn';
import colors from '../constants/Colors';

export default class RequestListScreen extends React.Component {

	state = {
		data: [],
		refreshing: false,
		sections: [],
	}

	static navigationOptions = ({ navigation }) => {
		return {
			headerStyle: { 
				marginTop: -1 * StatusBar.currentHeight,
				backgroundColor: colors.appHeaderBgColor,
			},
			titleStyle: {
				color: '#ffffffcc'
			},
			tintColor: '#ffffffcc',
			title: "Requests",
			headerLeft: null,			
		}
	}

	componentDidMount() {
		this._sub = this.props.navigation.addListener(
			'didFocus',
			this._onPageFocus_Reload
		)
		this._onPageFocus_Reload()
	}

	render() {
		return (
			<View style={styles.container}>
				<ScrollView 
					style={styles.container} 
					contentContainerStyle={styles.contentContainer}
					refreshControl={
						<RefreshControl
						  refreshing={this.state.refreshing}
						  onRefresh={this._onRefresh.bind(this)}
						/>
					}
				>
					<View>
						{/* <FlatList
							style={styles.bigList}
							data={this.state.data}
							renderItem={({ item }) => this.xItem(item)}
						/> */}
						<SectionList
							style={styles.bigList}
							sections={this.state.sections}
							renderItem={ ({ item }) => this.xItem(item) }
							renderSectionHeader={ ({ section }) => (<View
								style={{
									// borderBottomWidth: 2, //StyleSheet.hairlineWidth,
									// borderBottomColor: colors.appLinesColor,
									// borderStyle: 'dotted',
									paddingBottom: 10,
									// borderRadius: 6,
									// borderTopWidth: StyleSheet.hairlineWidth,
									// borderTopColor: colors.appLinesColor,
								}}>
								<Text style={{
									color: colors.appH1Color,
									fontSize: 26,
									paddingLeft: 15,
									paddingTop: 10,
									// fontStyle: 'italic',
									fontWeight: '800',
									}}>
									{ ((section.title!='undefined')?section.title:'(Other)')}
								</Text>
							</View>) }
						/>
					</View>
				</ScrollView>
			</View>
		);
	}

	_onPageFocus_Reload = () => {
		return getAllRequests()
			.then(data => {
				let i = 0
				data.forEach(element => {
					element.key = ++i
					if (element.locName) {
						// ok.
					} else {
						let addressItems = (element.locAddress||'').split(',')
						element.locName = addressItems[1] || addressItems[0] || element.locAddress
					}
				});

				const _sections = this.groupBy(data, 'locName');
				const keys = Object.keys(_sections);
				let sections = keys.map(key => {return {title:key, data:_sections[key]}});
				sections.sort( (sectionA, sectionB) => - this._getWeightOfGroup(sectionA) + this._getWeightOfGroup(sectionB) );
				// sections.forEach( section => section.weight );

				// let other_section = sections.find(x => !x.title);
				// sections = sections.filter(x => x.title);
				// sections.push(other_section);

				this.setState({ data, sections });
			})
	}

	groupBy(data, key) {
		return data.reduce((rv, x) => {
			(rv[x[key]] = rv[x[key]] || []).push(x);
			return rv;
		}, {});
	}

	_getWeightOfGroup(section) {
		if (section.title=='undefined') {
			return -999;
		}
		
		const w = section.data.reduce( (total, item) => total + this._getWeightOfRequest(item), 0 );
		console.log(w, '<-- weight');
		return w;
	}

	_getWeightOfRequest(request) {
		return (+request.numMen)*10 
		+ (+request.numLadies)*15 
		+ (+request.numSick)*20 
		+ (+request.numDisabled)*25 
		+ (+request.numChildrenAbove5)*30 
		+ (+request.numChildrenBelow5)*35 
		+ (+request.numPregnant)*40;
	}

	xCheckmark(showOrNot) {
		if (showOrNot) {
			return (
				<Ionicons
					name="ios-checkmark-circle"
					size={18}
					color={colors.verifyBadgeColor}
					style={{
						paddingTop: 3,
						paddingRight: 5,
						paddingTop: 20,
					}}
				/>
			)
		} else {
			return null;
		}
	}

	xItem = (item) => (
		<View style={styles.bigListItem}>
			<View style={styles.bigListItemAllTextContainer}>
				{/* <Text style={styles.bigListItemHeader}>{item.locName}</Text> */}
				<View style={{ flexDirection: 'row' }}>
					{this.xCheckmark(item.accepted)}
					<Text style={styles.bigListItemSubtext}>{item.name}</Text>
				</View>
			</View>
			<View style={styles.bigListItemButtonsContainer}>
				<Btn
					title="View"
					bgColor={(item.accepted)?colors.btnBgPrimaryColor:colors.btnBgColor} fgColor={colors.btnFgColor}
					onPress={(x) => this.props.navigation.navigate("ViewRequest", { item, loggedIn: true,})}
				/>
			</View>
		</View>
	);

	_onRefresh() {
		this.setState({refreshing: true});
		this._onPageFocus_Reload().then(() => {
		  this.setState({refreshing: false});
		});
	  }

}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: colors.appBgColor,
	},
	contentContainer: {
		padding: 20,
		paddingTop: 10,
		paddingHorizontal: 0,
	},
	bigList: {
		paddingHorizontal: 0,
	},
	bigListItem: {
		height: 55,
		paddingHorizontal: 20,
		borderBottomWidth: StyleSheet.hairlineWidth,
		borderBottomColor: colors.appLinesColor,
		flexDirection: 'row',
		justifyContent: 'space-between',
	},
	bigListItemHeader: {
		flexDirection: 'row',
		fontSize: 20,
		paddingTop: 3,
		fontWeight: '100',
	},
	bigListItemSubtext: {
		flexDirection: 'row',
		fontSize: 18,
		paddingBottom: 7,
		paddingTop: 16,
		color: colors.appH2Color,
	},
	bigListItemAllTextContainer: {
		flexDirection: 'column',
		justifyContent: 'flex-start',
	},
	bigListItemButtonsContainer: {
		flexDirection: 'column',
		justifyContent: 'center',
		height: 55,
	},
});

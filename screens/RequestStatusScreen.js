import React from 'react';
import {
	Image,
	Platform,
	ScrollView,
	StyleSheet,
	Text,
	View,
	FlatList,
	Button,
	StatusBar,
	RefreshControl,
} from 'react-native';

export default class RequestStatusScreen extends React.Component {
    static navigationOptions = ({ navigation }) => {
		return {
			headerStyle: { marginTop: -1 * StatusBar.currentHeight },
			title: "New request",
			headerLeft: null,
		}
    }
    
    render() {
        return <View style={styles.container}>
            <Text>Hahaha</Text>
        </View>
    }
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: '#fff',
	},
	contentContainer: {
		padding: 20,
		paddingTop: 10,
		paddingHorizontal: 0,
	},
});
import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  Alert,
} from 'react-native';

import { MonoText } from '../components/StyledText';

import { signUp } from '../api/Auth'

import { Btn } from '../components/Btn';
import colors from '../constants/Colors';

export default class SignupScreen extends React.Component {
  static navigationOptions = {
    header: null,
    // tabBarVisible: false,
    title: "Sign up",
  };

  state = {
    nae: null,
    pass: null,
    email: null,
    pass2: null,
  }

  // _signup() {
  //   Alert.alert(
  //     'Registration successful',
  //     'Use your email address and password to log in.',
  //     [
  //       {text: 'OK', onPress: () => null},
  //     ],
  //     { cancelable: false }
  //   );

  //   this.props.navigation.navigate('RequestList', {loggedIn:true,});
  // }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
          <View style={styles.formContainer}>
            <Image
              style={{width: 172, height: 44}}
              source={require('../assets/images/Donate-lk-text-1.png')}
            />
            <Text style={styles.formHeader}>Sign up</Text>
            <View style={{margin:9}} />
            {/* <Text>
              Provide your email and a choice of password to register.
            </Text> */}
            <View style={{margin:12}} />
            <TextInput value={this.state.name} onChangeText={(name)=>this.setState({name})} placeholder='Name' style={styles.loginInput} underlineColorAndroid='transparent' placeholderTextColor='#aaa' />
            <TextInput value={this.state.email} onChangeText={(email)=>this.setState({email})} placeholder='E-mail' style={styles.loginInput} underlineColorAndroid='transparent' placeholderTextColor='#aaa' />
            <TextInput value={this.state.pass} onChangeText={(pass)=>this.setState({pass})} placeholder='Password' secureTextEntry={true} style={styles.loginInput} underlineColorAndroid='transparent' placeholderTextColor='#aaa' />
            <TextInput value={this.state.pass2} onChangeText={(pass2)=>this.setState({pass2})} placeholder='Password' secureTextEntry={true} style={styles.loginInput} underlineColorAndroid='transparent' placeholderTextColor='#aaa' />
            <View style={{margin:9}} />
            <Btn 
                onPress={(x)=>{
                  if (this.state.email && this.state.pass && this.state.pass2) {
                    signUp(this.state.name, this.state.email, this.state.pass)
                      .then(isLogInSuccess => {
                        if (isLogInSuccess) {

                          Alert.alert(
                            'Registration successful',
                            'Use your email address and password to log in.',
                            [
                              {text: 'OK', onPress: () => null},
                            ],
                            { cancelable: false }
                          );

                          this.props.navigation.navigate("Login");

                        } else {
                          //TODO: Show username/password mismatch error
                        }
                      }).catch(err => {
                        //TODO: Show username/password mismatch error
                      })
                    // this._signup();
                    // this.props.navigation.navigate("Login");
                  }
                }}
                title="Sign up"
                color="#1982C4"
                fgColor="#ffffff"
            />
            <View style={{margin:12}} />
            <Text style={{color: '#ccc'}}>
              If already have an account, go back to sign in.
            </Text>
             <View style={{margin:9}} />
            <Btn 
                onPress={(x)=>this.props.navigation.navigate("Login")}
                title="← Go back"
                color="#6A4C93"
                fgColor="#ffffff"
            /> 
          </View>
        </ScrollView>
        <View style={styles.tabBarInfoContainer}>
          <Text style={{paddingHorizontal: 10, color: '#ccc'}}>Contact us if you have any problems signing in.</Text>

          <View>
            <MonoText style={styles.codeHighlightText}>support@donate.lk</MonoText>
          </View>
        </View>
      </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#333',
  },
  contentContainer: {
    padding: 20,
    paddingTop: 30,
  },
  formContainer: {
    paddingVertical: 20,
    alignItems: 'center',
  },
  formHeader: {
    fontSize: 24,
    color: '#fff',
  },
  loginInput: {
    fontSize: 16,
    padding: 5,
    lineHeight: 22,
    minWidth: 150,
    textAlign: 'center',
    color: '#fff',
    backgroundColor: '#ffffff33',
    borderRadius: 4,
    margin: 3,
  },
  codeHighlightText: {
    color: 'rgba(255, 255, 255, 0.8)',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
    backgroundColor: '#333',
  },
});

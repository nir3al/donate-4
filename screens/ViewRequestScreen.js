import React from 'react';
import {
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  View,
  Button,
  StatusBar,
  TextInput,
  Picker,
  Alert,
  Image,
} from 'react-native';

import { WebBrowser } from 'expo';

import getDirections from 'react-native-google-maps-directions'
import { WeatherWidget } from 'react-native-weather';

import { updateRequest } from '../api/Request'

import { Btn } from '../components/Btn';
import colors from '../constants/Colors';

export default class ViewRequestScreen extends React.Component {

  state = {
  };

  static navigationOptions = ({ navigation }) => {
    return {
      headerStyle: { 
				marginTop: -1 * StatusBar.currentHeight,
				backgroundColor: colors.appHeaderBgColor,
			},
      tabBarVisible: true,
      title: "Request details",
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
          <View style={styles.formContainer}>

            {this.xWeatherWidget()}

            <View style={{
              flex: 1,
              flexDirection: 'row',
              flexWrap: 'wrap',
              marginBottom: 10,
            }}>
              {this.btnOpenPhoto()}
              {this.btnOpenDirections()}
              {this.btnOpenWeather()}
            </View>

            <View style={styles.formEntry}>
              <Text style={styles.formEntrySubsectionHeader}>Who made the request</Text>
              <Text style={styles.label}>{this.props.navigation.state.params.item.name || '—'}</Text>
            </View>

            {/* <View style={styles.formEntry}>
              <Text style={styles.formEntrySubsectionHeader}>Where it is from</Text>
              <Text style={styles.label}>{this.props.navigation.state.params.item.location || '—'}</Text>
            </View> */}

            <View style={styles.formEntry}>
              <Text style={styles.formEntrySubsectionHeader}>Are they safe?</Text>
              <Text style={styles.label}>{this._getSafeStateAsText()}</Text>
            </View>

            <View style={styles.formEntry}>
              <Text style={styles.formEntrySubsectionHeader}>Verification code</Text>
              <Text style={styles.label}>{this.props.navigation.state.params.item.verifCode || '—'}</Text>
            </View>

            <View style={styles.formEntry}>
              <Text style={styles.formEntrySubsectionHeader}>Contact number</Text>
              <Text style={styles.label}>{this.props.navigation.state.params.item.phone || '—'}</Text>
            </View>

            <View style={styles.formEntry}>
              <Text style={styles.formEntrySubsectionHeader}>How many people are over there</Text>
              <View style={styles.formSubSection}>
                <View style={styles.formEntry}>

                  <Text style={styles.formEntryHeader}>Children below 5</Text>
                  <Text style={styles.label}>{this.props.navigation.state.params.item.numChildrenBelow5}</Text>

                  <Text style={styles.formEntryHeader}>Children above 5</Text>
                  <Text style={styles.label}>{this.props.navigation.state.params.item.numChildrenAbove5}</Text>

                  <Text style={styles.formEntryHeader}>Pregnant mothers</Text>
                  <Text style={styles.label}>{this.props.navigation.state.params.item.numPregnant}</Text>

                  <Text style={styles.formEntryHeader}>Sick people</Text>
                  <Text style={styles.label}>{this.props.navigation.state.params.item.numSick}</Text>

                  <Text style={styles.formEntryHeader}>Disabled people</Text>
                  <Text style={styles.label}>{this.props.navigation.state.params.item.numDisabled}</Text>

                  <Text style={styles.formEntryHeader}>Other women</Text>
                  <Text style={styles.label}>{this.props.navigation.state.params.item.numLadies}</Text>

                  <Text style={styles.formEntryHeader}>Other men</Text>
                  <Text style={styles.label}>{this.props.navigation.state.params.item.numMen}</Text>

                </View>
              </View>
            </View>

            <View style={styles.formEntry}>
              <Text style={styles.formEntrySubsectionHeader}>Comments:</Text>
              <Text style={styles.label}>{this.props.navigation.state.params.item.comments || "(None)"}</Text>
            </View>

            <View style={{
              flex: 1,
              flexDirection: 'row',
              flexWrap: 'wrap',
              marginTop: 15,
            }}>
              {this.btnAcceptReq()}
              <Btn title="Close" bgStyle={styles.btnListItem} onPress={() => this.props.navigation.navigate("RequestList", { loggedIn: true, })} />
            </View>

            <View style={styles.formEndEmptySpace}></View>
          </View>
        </ScrollView>
      </View>
    );
  }

  btnOpenDirections() {
    if (this.props.navigation.state.params.item.accepted) {
      return (<Btn title="Show directions" color={colors.btnBgPrimaryColor} fgColor={colors.btnFgColor} bgStyle={styles.btnListItem} onPress={() => this._handleGetDirections()} />);
    } else {
      return null;
    }
  }

  btnOpenWeather() {
    if (this.props.navigation.state.params.item.accepted) {
      return (<Btn title="Week's weather" color={colors.btnBgColor} fgColor={colors.btnFgColor} bgStyle={styles.btnListItem} onPress={() => this._showWeatherPage()} />)
    } else {
      return null;
    }
  }

  btnOpenPhoto = () => {
    return (<Btn title="View photo" color={colors.btnBgColor} fgColor={colors.btnFgColor} bgStyle={styles.btnListItem} onPress={() => {
        this.props.navigation.navigate("ViewRequestPhoto", {item:this.props.navigation.state.params.item, reqPeopleCount:this.getReqPeopleCount()})
      }} />);
  }

  getReqPeopleCount() {
    return this.props.navigation.state.params.item.numChildrenBelow5 + this.props.navigation.state.params.item.numChildrenAbove5 + this.props.navigation.state.params.item.numDisabled
      + this.props.navigation.state.params.item.numLadies + this.props.navigation.state.params.item.numMen + this.props.navigation.state.params.item.numPregnant + this.props.navigation.state.params.item.numSick; 
  }

  xWeatherWidget() {
    // if (this.props.navigation.state.params.item.accepted) {
    //   return (
    //     <WeatherWidget
    //       api={"d56f8cb05558b141e61a50d32b3bb8b0"}
    //       lat={6.9934}
    //       lng={81.0550}
    //     />
    //   );
    // } else {
    //   return null;
    // }
  }

  btnAcceptReq() {
    return this.btnSeeItems();
  }

  btnSeeItems() {
    return ((this.props.navigation.state.params.item.accepted)?<Btn
      title="View required items"
      bgColor={colors.btnBgPrimaryColor}
      bgStyle={styles.btnListItem}
      onPress={()=>{
        console.log(this.props.navigation.state.params.item)
        this.props.navigation.navigate("AcceptRequest", {request: this.props.navigation.state.params.item, isAccepted:true})
      }}
    />:<Btn
      title="Accept request"
      bgColor={colors.btnBgPrimaryColor}
      bgStyle={styles.btnListItem}
      onPress={()=>{
        console.log(this.props.navigation.state.params.item)
        this.props.navigation.navigate("AcceptRequest", {request: this.props.navigation.state.params.item, isAccepted:false})
      }}
    />)
  }

  _showWeatherPage() {
    this.props.navigation.navigate("Weather", this.props.navigation.state.params);
  }

  ___handleGetDirections = () => {
    const data = {
      destination: {
        latitude: 6.9934,
        longitude: 81.0550
      },
      params: [
        {
          key: "dirflg",
          value: "w"
        }
      ]
    }

    getDirections(data)
  }

  _handleGetDirections = () => {
    this.props.navigation.navigate("Directions", {
      lat:this.props.navigation.state.params.item.locLat, 
      long:this.props.navigation.state.params.item.locLong,
      address:this.props.navigation.state.params.item.locAddress,
    });
  }

  _getLocationAsync = async () => {
    this.props.navigation.navigate("LocationPicker", this.props.navigation.state.params);
  };

  _handleOpenWithWebBrowser = (url) => {
    WebBrowser.openBrowserAsync(url);
  }

  _acceptRequest() {
    this.props.navigation.navigate("AcceptRequest", {request: this.state.request, isAccepted:false})
  }

  ___acceptRequest() {
    let _item = Object.assign({}, this.props.navigation.state.params.item)
    _item.accepted = true
    _item.acceptedBy = __user._id
    updateRequest(_item)

    Alert.alert(
      "Request accepted",
      'Thank you!',
      [
        { text: 'OK', onPress: () => null },
      ],
      { cancelable: false, }
    )

    this.props.navigation.navigate('RequestList', { loggedIn: true, });
  }

  _getSafeStateAsText() {
    const safety = this.props.navigation.state.params.item.victimStatus
    if (safety == 'safe') return 'Safe'
    else return 'Not safe'
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.appBgColor,
  },
  contentContainer: {
    padding: 20,
    paddingTop: 10,
    paddingHorizontal: 0,
  },
  btnListItem: {
    marginRight: 8,
    marginVertical: 4,
  },
  formContainer: {
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  formEntry: {
    paddingBottom: 6,
  },
  formActionAtEnd: {
    marginVertical: 15,
  },
  formEntryHeader: {
    marginVertical: 5,
    color: colors.appBodyFgColor2,
  },
  formEntrySubsectionHeader: {
    fontWeight: "500",
    paddingBottom: 5,
    color: colors.appBodyFgColor2,
  },
  label: {
    fontSize: 16,
    paddingVertical: 5,
    lineHeight: 22,
    paddingHorizontal: 10,
    borderRadius: 4,
    overflow: "hidden",
    backgroundColor: '#ffffff1a',
    color: colors.appBodyFgColor,
    marginHorizontal: 20,
  },
  inputPickerWrapper: {
    marginVertical: 5,
    ...Platform.select({
      android: {
        borderBottomColor: 'grey',
        borderBottomWidth: 1,
      },
    }),
  },
  inputPicker: {
    height: 26,
  },
  inputPickerItem: {
    textAlign: 'center',
  },
  formSubSection: {
    paddingHorizontal: 20,
  },
  formEntryRequiredNote: {
    color: '#881111',
    fontSize: 12,
  },
  formEndEmptySpace: {
    height: 200,
  },
});

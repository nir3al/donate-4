import React, { Component } from 'react';
import {
	StyleSheet,
	Text,
	View,
	TouchableOpacity,
	StatusBar,
	Button,
	Image,
	Alert,
} from 'react-native';

import { Constants, Location, Permissions } from 'expo';

import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';

export default class LocationPickerScreen extends Component {

	static navigationOptions = ({ navigation }) => {
		return {
			headerStyle: { marginTop: -1 * StatusBar.currentHeight },
			title: "Location",
			headerRight: (
				<View style={{ paddingRight: 15 }}>
					<Button
						title="Open Map"
						color="#841584"
						onPress={() => navigation.navigate("Map", this.props.navigation.state.params)}
					/>
				</View>
			)
		}
	}

	state = {
		location: null,
		lat: null,
		long: null,
		locationName: null,
		locationId: null,
		locationDescription: null,
		placeId: null,
		errorMessage: null,
	};

	render() {
		return (
			<View style={styles.container}>
				<GooglePlacesAutocomplete
					placeholder='Show us your location'
					minLength={2} // minimum length of text to search
					autoFocus={false}
					returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
					listViewDisplayed='auto'    // true/false/undefined
					fetchDetails={true}
					renderDescription={row => {
						return row.description || row.name;
					}} // custom description render
					onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
						if (data.geometry) {
							this.setState({ placeId: null, locationId: null, locationDescription: null, });
							this.setState({
								lat: data.geometry.location.lat,
								long: data.geometry.location.lng,
								locationName: data.name,
							});
							this.props.navigation.state.params.setWrapperState({
								lat: this.state.lat,
								long: this.state.long,
								name: this.locationName,
							})
						} else {
							this.setState({
								placeId: data.place_id,
								locationId: data.location_id,
								locationDescription: data.description,
							});
							this.setState({ lat: null, long: null, locationName: null, });
							this.props.navigation.state.params.setWrapperState({
								placeId: this.state.lat,
								locationId: this.state.long,
								name: this.locationName,
							})
						}
					}}

					getDefaultValue={() => ''}

					query={{
						// available options: https://developers.google.com/places/web-service/autocomplete
						key: 'AIzaSyDioBGXRFyzk2dzZvlpSMIIKBpDGIXWSaA',
						language: 'en', // language of the results
						types: '(cities)' // default: 'geocode'
					}}

					styles={{
						textInputContainer: {
							width: '100%'
						},
						description: {
							fontWeight: 'bold'
						},
						predefinedPlacesDescription: {
							color: '#1faadb'
						}
					}}

					currentLocation={true} // Will add a 'Current location' button at the top of the predefined places list
					currentLocationLabel="Current location"
					nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
					GoogleReverseGeocodingQuery={{
						// available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
					}}
					GooglePlacesSearchQuery={{
						// available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
						rankby: 'distance',
						types: 'food'
					}}

					filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
					//predefinedPlaces={[homePlace, workPlace]}

					debounce={200} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
				// renderLeftButton={()  => <Image source={require('path/custom/left-icon')} />}
				// renderRightButton={() => <Text>Custom text after the input</Text>}
				/>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#fff',
	},
	location: {
		backgroundColor: 'white',
		margin: 25
	}
});
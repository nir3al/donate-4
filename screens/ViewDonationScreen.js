import React from 'react';
import {
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  View,
  Button,
  StatusBar,
  TextInput,
  Picker,
  Alert,
  Image,
} from 'react-native';

import { Btn } from '../components/Btn';
import colors from '../constants/Colors';

export default class ViewDonationScreen extends React.Component {

  state = {
    victimStatus: "safe",
    numChildrenBelow5: "",
    numChildrenAbove5: "",
    numPregnant: "",
    numDisabled: "",
    numLadies: "",
    numMen: "",
  };

  static navigationOptions = ({navigation}) => {
    return {
      headerStyle: { 
				marginTop: -1 * StatusBar.currentHeight,
				backgroundColor: colors.appHeaderBgColor,
			},
      tabBarVisible: true,
      title: "Donation details",
      //headerRight: (
      //  <View style={{paddingRight: 15}}>
      //    <Button 
      //      title="Accept" 
      //      color="#841584"
      //      onPress={() => {
      //        Alert.alert(
      //          'Request accepted',
      //          'You will be notified on how to proceed.',
      //          [
      //            {text: 'OK', onPress: () => null},
      //          ],
      //          { cancelable: false }
      //        );
      //
      //        navigation.navigate('RequestList');
      //      }}
      //    />
      //  </View>
      //)
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
          <View style={styles.formContainer}>

            <View style={styles.formEntry}>
              <Text style={styles.formEntrySubsectionHeader}>Who made the donation</Text>
              <Text style={styles.label}>{this.props.navigation.state.params.item.name}</Text>
            </View>

            <View style={styles.formEntry}>
              <Text style={styles.formEntrySubsectionHeader}>Where it was located</Text>
              <Text style={styles.label}>{this.props.navigation.state.params.item.address}</Text>
            </View>

            <View style={styles.formEntry}>
              <Text style={styles.formEntrySubsectionHeader}>When it was retrieved</Text>
              <Text style={styles.label}>{this.props.navigation.state.params.item.date}</Text>
            </View>
            
            <View style={styles.formEntry}>
              <Text style={styles.formEntrySubsectionHeader}>Donated items</Text>
              <View style={styles.formSubSection}>
                <View style={styles.formEntry}>

                  <Text style={styles.formEntryHeader}>{this.props.navigation.state.params.item.type}</Text>
                  <Text style={styles.label}>{this.props.navigation.state.params.item.amount} {this.props.navigation.state.params.item.units} of {this.props.navigation.state.params.item.category}</Text>
                  {/* , 4 litres of Small */}
                  {/*
                  <Text style={styles.formEntryHeader}>Food</Text>
                  <Text style={styles.label}>15 kg of Rice and curry, 5 kg of Biscuits</Text>

                  <Text style={styles.formEntryHeader}>Clothes</Text>
                  <Text style={styles.label}>For men, women and kids</Text> */}

                </View>
              </View>
            </View>
            
            <View style={styles.formEndEmptySpace}></View>
          </View>
        </ScrollView>
      </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.appBgColor,
  },
  contentContainer: {
    padding: 20,
    paddingTop: 10,
    paddingHorizontal: 0,
  },
  formContainer: {
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  formEntry: {
    paddingBottom: 6,
  },
  formActionAtEnd: {
    paddingVertical: 12,
  },
  formEntryHeader: {
    paddingBottom: 5,
    color: colors.appBodyFgColor,
  },
  formEntrySubsectionHeader: {
    fontWeight: "500",
    paddingBottom: 5,
    color: colors.appBodyFgColor2,
  },
  label: {
    fontSize: 16,
    paddingVertical: 5,
    lineHeight: 22,
    paddingHorizontal: 10,
    borderRadius: 4,
    overflow: "hidden",
    backgroundColor: '#ffffff1a',
    color: colors.appBodyFgColor,
    marginHorizontal: 20,
  },
  inputPickerWrapper: {
    marginVertical: 5,
    ...Platform.select({
      android: {
        borderBottomColor: 'grey',
        borderBottomWidth: 1,
      },
    }),
  },
  inputPicker: {
    height: 26,
  },
  inputPickerItem: {
    textAlign: 'center',
  },
  formSubSection: {
    paddingHorizontal: 20,
  },
  formEntryRequiredNote: {
    color: '#881111',
    fontSize: 12,
  },
  formEndEmptySpace: {
    height: 200,
  },
});

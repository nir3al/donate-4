import React, { Component } from 'react';
import {
	StyleSheet,
	Text,
	View,
	StatusBar,
    Button,
    TextInput,
    Alert,
    Platform,
} from 'react-native';

import { Constants, MapView, Location, Permissions } from 'expo';

import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';

import { getPlaceData } from '../api/Places'

import {Btn} from '../components/Btn'
import colors from '../constants/Colors'

export default class LocationPicker2Screen extends Component {

    mapView = null;

	static navigationOptions = ({ navigation }) => {
		return {
			headerStyle: { marginTop: -1 * StatusBar.currentHeight },
			title: "Location",
		}
	}

	state = {
        mapRegion: { latitude: 6.9147, longitude: 79.9733, latitudeDelta: 0.00922, longitudeDelta: 0.00421 },
        location: { latitude: 6.9147, longitude: 79.9733 },
        errorMessage: null,
        text: '',
        isBusy: false,
    };
    
    _handleMapRegionChange = mapRegion => {
        this.setState({ mapRegion });
        this.props.navigation.state.params.setWrapperState({
            lat: this.state.mapRegion.latitude,
            long: this.state.mapRegion.longitude,
            name: '',
        })
    };

    componentWillMount() {
        if (Platform.OS === 'android' && !Constants.isDevice) {
            this.setState({
                errorMessage: 'Oops, this will not work on Sketch in an Android emulator. Try it on your device!',
            });
        } 
        else {
            this._loadUserLocation();
        }
    }

    _loadLocationAsync = async () => {
        let { status } = await Permissions.askAsync(Permissions.LOCATION);
        if (status !== 'granted') {
            this.setState({
                errorMessage: 'Permission to access location was denied',
            });
        }
    
        let location = await Location.getCurrentPositionAsync({});

        // Alert.alert(
        //     'Location acquired',
        //     JSON.stringify(location),
        //     [
        //         { text: 'OK', onPress: () => null },
        //     ],
        //     { cancelable: false }
        // )

        if (location) {
            this.setState({
                location,
                mapRegion: {
                    latitude: location.coords.latitude,
                    longitude: location.coords.longitude,
                    latitudeDelta: this.state.mapRegion.latitudeDelta,
                    longitudeDelta: this.state.mapRegion.longitudeDelta,
                }
            });

            this.props.navigation.state.params.setWrapperState({
                lat: this.state.lat,
                long: this.state.long,
                name: this.state.locationName,
            })
        }

        return location;
    };

    _searchByText = () => {}

    _loadUserLocation = () => {
        const loc = this._loadLocationAsync();
        this._setRegion(loc);
    }

    _setRegion(region) {
        if(this.state.ready) {
          setTimeout(() => this.map.mapview.animateToRegion(region), 10);
        }
        //this.setState({ region });
    }

    _setStateForLocations(data) {
        this.setState({ placeId: null, locationId: null, locationDescription: null, });
        this.setState({
            lat: data.geometry.location.lat,
            long: data.geometry.location.lng,
            locationName: data.name,
            
            mapRegion: {
                latitude: data.geometry.location.lat,
                longitude: data.geometry.location.lng,
                latitudeDelta: this.state.mapRegion.latitudeDelta,
                longitudeDelta: this.state.mapRegion.longitudeDelta,
            }
        });
        this.props.navigation.state.params.setWrapperState({
            lat: this.state.lat,
            long: this.state.long,
            name: this.state.locationName,
        })
    }

	render() {
		return (
			<View style={styles.container}>

                <View style={styles.buttonGroup}>
                    <Btn style={styles.buttonInGroup} title="Current" bgColor='#FFCA3A' fgColor={colors.btnFgColor} onPress={this._loadUserLocation} />
                </View>

                <MapView
                    style={styles.mapView}
                    region={this.state.mapRegion}
                    provider={MapView.PROVIDER_GOOGLE}
                    onRegionChange={this._handleMapRegionChange}
                    showsUserLocation={true}
                    followsUserLocation={true}
                    ref={ map => { this.mapView = map }}
                    >
                    <MapView.Marker
                        coordinate={this.state.mapRegion}
                        title="My location"
                        description="Position me at your nearest location"
                        />
                </MapView>

                <GooglePlacesAutocomplete
					placeholder='Show us your location'
					minLength={2} // minimum length of text to search
					autoFocus={false}
					returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
					listViewDisplayed='auto'    // true/false/undefined
					fetchDetails={true}
					renderDescription={row => {
						return row.description || row.name;
					}} // custom description render
                    onPress={(data, details = null) => { // 'details' is provided when fetchDetails = true
                        this.setState({isBusy:true})
						if (data.geometry) {
							// this.setState({ placeId: null, locationId: null, locationDescription: null, });
							// this.setState({
							// 	lat: data.geometry.location.lat,
							// 	long: data.geometry.location.lng,
                            //     locationName: data.name,
                                
                            //     mapRegion: {
                            //         latitude: data.geometry.location.lat,
                            //         longitude: data.geometry.location.lng,
                            //         latitudeDelta: this.state.mapRegion.latitudeDelta,
                            //         longitudeDelta: this.state.mapRegion.longitudeDelta,
                            //     }
                            // });
							// this.props.navigation.state.params.setWrapperState({
							// 	lat: this.state.lat,
							// 	long: this.state.long,
							// 	name: this.locationName,
                            // })
                            // Alert.alert(
                            //     'Location acquired',
                            //     `late=${this.state.lat}, `,
                            //     [
                            //         { text: 'OK', onPress: () => null },
                            //     ],
                            //     { cancelable: false }
                            // )
                            this._setStateForLocations(data);
                            this.setState({isBusy:false})
						} else {
							// this.setState({
							// 	placeId: data.place_id,
							// 	locationId: data.location_id,
							// 	locationDescription: data.description,
							// });
							// this.setState({ lat: null, long: null, locationName: null, });
							// this.props.navigation.state.params.setWrapperState({
							// 	placeId: this.state.lat,
							// 	locationId: this.state.long,
							// 	name: this.locationName,
                            // })
                            // Alert.alert(
                            //     'Please wait...',
                            //     "Acquiring location",
                            //     [
                            //         { text: 'OK', onPress: () => null },
                            //     ],
                            //     { cancelable: false }
                            // )
                            getPlaceData(data.place_id)
                                .then(json => {
                                    data.geometry = json.result.geometry;
                                    this._setStateForLocations(data);
                                    // Alert.alert(
                                    //     'Location updated',
                                    //     '',
                                    //     [
                                    //         { text: 'OK', onPress: () => null },
                                    //     ],
                                    //     { cancelable: false }
                                    // )
                                    this.setState({isBusy:false})
                                    //TODO: go to the request status page
                                })
                        }
					}}

					getDefaultValue={() => ''}

					query={{
						// available options: https://developers.google.com/places/web-service/autocomplete
						key: 'AIzaSyDioBGXRFyzk2dzZvlpSMIIKBpDGIXWSaA',
						language: 'en', // language of the results
						types: '(cities)' // default: 'geocode'
					}}

					styles={{
						container: {
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            right: 0,
                            width: '100%',
                        },
                        listView: {
                            backgroundColor: '#ffffff'
                        },
                        textInputContainer: {
                            width: '100%'
                        },
                        textInput: {
                        },
                        description: {
                            fontWeight: 'bold'
                        },
                        predefinedPlacesDescription: {
                            color: '#1faadb'
                        },
                        poweredContainer: {
                            display: 'none',
                        }
					}}

					currentLocation={false} // Will add a 'Current location' button at the top of the predefined places list
					currentLocationLabel="Current location"
					nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
					GoogleReverseGeocodingQuery={{
						// available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
					}}
					GooglePlacesSearchQuery={{
						// available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
						rankby: 'distance',
						types: 'food'
					}}

					filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities
					//predefinedPlaces={[homePlace, workPlace]}

					debounce={200} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
				// renderLeftButton={()  => <Image source={require('path/custom/left-icon')} />}
				// renderRightButton={() => <Text>Custom text after the input</Text>}
				/>

                {(this.state.isBusy)?<View style={styles.overlay}></View>:null}
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
        backgroundColor: '#fff',
        // paddingTop: Constants.statusBarHeight,
    },
    mapView: { 
        flex: 1,
        alignSelf: 'stretch', 
        // height: 600,
        marginBottom: -30,
    },
    buttonGroup: {
        flexDirection: 'row',
        // justifyContent: 'space-between',
        marginTop: 55,
        marginBottom: 10,
        alignItems: 'flex-end',
    },
    buttonInGroup: {
        backgroundColor: 'green',
        // width: '40%',
        height: 40
    },
    inputInButtonGroup: {
        minWidth: 120,
    },
    overlay: {
        flex: 1,
        backgroundColor: 'black',
        // width: width
        position: 'absolute',
        top: 0,
        right: 0,
        bottom: 0,
        left: 0,
        opacity: 0.3
    },
});
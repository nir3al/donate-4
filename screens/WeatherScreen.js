import React from 'react';
import {
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  View,
  Button,
  StatusBar,
  TextInput,
  Picker,
  Alert,
  Image,
} from 'react-native';

import { Ionicons } from '@expo/vector-icons';

import { getWeatherData, getRainfallOfWeek } from '../api/Weather'

export default class WeatherScreen extends React.Component {

  state = {
    // day: null,
    days: [],
    date: null,
    month: null,
    time: null,
    timerTaskId: null,
    isWeatherDataLoaded: false,
    timezone: {},
    currentWeather: {},
    weekWeather: [],
    weekWeatherSummary: {},
    rainfall: []
  };

  static navigationOptions = ({navigation}) => {
    return {
      headerStyle: { 
        marginTop: -1 * StatusBar.currentHeight,
        backgroundColor: '#6CAFD9',
      },
      title: "Week's weather",
    }
  }

  componentDidMount() {
    this._bindDateTime();
    // "https://api.darksky.net/forecast/d56f8cb05558b141e61a50d32b3bb8b0/37.8267,-122.4233"
    // this.props.navigation.state.params.item
    getWeatherData(this.props.navigation.state.params.item.locLat, this.props.navigation.state.params.item.locLong).then(dataObj => {
      this.setState({
        timezone: dataObj.timezone, //obj
        currentWeather: dataObj.currently, //obj
        weekWeather: dataObj.daily.data, //array
        weekWeatherSummary: {summary:dataObj.daily.summary,icon:dataObj.daily.icon},
        isWeatherDataLoaded: true,
      })
    })

    let date = new Date();
    let yy = date.getFullYear()-1;
    let mm = date.getMonth() + 1;
    let dd = date.getDate();

    getRainfallOfWeek(yy,mm,dd).then(dataObj => {
      this.setState({rainfall:dataObj});
      console.log(this.state.rainfall);
    });

  }

  _bindDateTime() {
    let now = new Date();

    var days = ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'];
    var months = ['January','February','March','April','May','June','July','August','September','October','November','December'];

    this.setState({
      date: now.getDate(),
      month: months[now.getMonth()],
      // day: days[now.getDay()],
    });

    let _day = now.getDay() - 1;
    console.log('date idx', _day)
    let days_1 = days.slice(_day);
    // let days_2 = days.slice(0, _day);

    this.setState({days: days_1.concat(days.slice(0, _day)),});

    // let _today = days[now.getDay()];
    // let _days = [];
    // for (let i=0; i <= 7; i++) {
    //   _days.push(days[(_today+i) % 7 || 7]);
    // }
    // this.setState({days: _days});
    // // days: days[now.getDay()],

    let sec45 = 1000 * 45;
    let timerFunc = () => {
      this.setState({
        time: `${now.getHours() % 12 || 12}:${now.getMinutes() % 60 || 60}`,
      });
    };

    setInterval(timerFunc, sec45);

    timerFunc();
  }

  render() {
    return (this.state.isWeatherDataLoaded) ?
    (<View style={styles.container}>
      <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
        <View style={styles.pageContainer}>
        
          <View style={{}}>
            <Text style={{
              fontSize: 32,
            }}>{this.props.navigation.state.params.item.locAddress}</Text>
            <Text style={{
              fontSize: 15,
            }}>{this.state.days[0]} {this.state.date} {this.state.month}</Text>
            <Text style={{
              fontSize: 15,
            }}>Data retrieved at {this.state.time}</Text> 
          </View>

          {this._xTodaysWeather(this.state.currentWeather, this.state.rainfall[0])}

          <View style={styles.bottomItemsList}>

            {this._xWeekDayWeather(this.state.weekWeather[0], 1, this.state.rainfall[1])}
            {this._xWeekDayWeather(this.state.weekWeather[1], 2, this.state.rainfall[2])}
            {this._xWeekDayWeather(this.state.weekWeather[2], 3, this.state.rainfall[3])}
            {this._xWeekDayWeather(this.state.weekWeather[3], 4, this.state.rainfall[4])}
            {this._xWeekDayWeather(this.state.weekWeather[4], 5, this.state.rainfall[5])}
            {this._xWeekDayWeather(this.state.weekWeather[5], 6, this.state.rainfall[6])}

          </View>

        </View>
      </ScrollView>
    </View>)
    :
    (<View>
      <Text style={styles.waitText}>Please wait while weather data is being retrieved...</Text>
    </View>)
  }

  _getIconName(iconDesc) {
    switch (iconDesc) {
      case 'clear-day':
        return 'sunny'
      case 'rain':
        return 'rainy'
      case 'wind':
        return 'flag'
      case 'cloudy':
        return 'cloudy'
      case 'partly-cloudy-day':
        return 'cloudy'
      case 'partly-cloudy-night':
        return 'cloudy-night'
      case 'thunderstorm':
        return 'thunderstorm'
      default:
        return 'cloudy'
    }
  }

  _xWeekDayWeather(dayWeather, dayIdx, metRainfall) {
    return (<View style={styles.bottomItem}>
      <Text style={styles.bottomItemDay}>{this.state.days[dayIdx]}</Text>
      <Ionicons
        name={"ios-"+this._getIconName(dayWeather.icon)+"-outline"}
        size={40}
        style={{
          paddingLeft: 5,
        }}
      />
      <Text>{dayWeather.temperatureHigh}°F</Text>
      <Text>{metRainfall && metRainfall['obs_val(mm)']}mm</Text>
    </View>)
  }

  _xTodaysWeather(dayWeather, rainfall) {
    return (<View style={{
      flexDirection: 'column',
      flex: 1,
    }}>

      <View style={{
        alignSelf: 'center',
      }}>
        <View style={{
          flexDirection: 'column',
        }}>
          <Ionicons
            name={"ios-"+this._getIconName(dayWeather.icon)+"-outline"}
            size={60}
            style={{
              paddingTop: 16,
              marginBottom: -8,
            }}
          />
          <Text style={{
            fontSize: 18,
          }}>{dayWeather.summary}</Text>
        </View>
      </View> 

      <View style={{
        alignSelf: 'center',
      }}>
        <Text style={{
          fontSize: 60,
          marginRight: -10,
        }}>{dayWeather.temperature}°F</Text>
      </View>

      <View style={{
        alignSelf: 'center',
        flexDirection: 'row',
      }}>
        <Text style={{
          fontSize: 14,
        }}>{dayWeather.windSpeed} MPH</Text>
        <Ionicons
          name="ios-flag"
          size={20}
          style={{
            paddingLeft: 5,
            marginRight: 15,
          }}
        />
        <Text style={{
          fontSize: 14,
        }}>{100*(+dayWeather.humidity)}%</Text>
        <Ionicons
          name="ios-umbrella"
          size={20}
          style={{
            paddingLeft: 5,
            marginRight: 15,
          }}
        />
        <Text style={{
          fontSize: 14,
        }}>{rainfall && +rainfall['obs_val(mm)']} mm</Text>
        <Ionicons
          name="ios-water"
          size={20}
          style={{
            paddingLeft: 5,
          }}
        />
      </View>
      
    </View>)
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#6CAFD9',
  },
  contentContainer: {
    paddingBottom: 20,
    paddingTop: 10,
    paddingHorizontal: 0,
  },
  pageContainer: {
    paddingHorizontal: 15,
  },
  bottomItemsList: {
    flex: 1,
    justifyContent: 'space-around',
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 35,
  },
  bottomItem: {
    flexDirection: 'column',
    alignItems: 'center',
  },
  bottomItemDay: {
    fontSize: 12,
  },
  waitText: {
    textAlign: 'center',
    marginTop: 10,
  },
});

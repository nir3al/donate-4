import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  StatusBar,
  Button,
  Image,
  Alert,
} from 'react-native';

import { Camera, Permissions } from 'expo';

export default class CameraScreen extends Component {

  state = {
    hasCameraPermission: null,
    type: Camera.Constants.Type.back,
  };

  static navigationOptions = ({navigation}) => {
    return {
      headerStyle: { marginTop: -1 * StatusBar.currentHeight },
      title: "Attach photo",
      //headerRight: (
      //  <View style={{paddingRight: 15}}>
      //    <Button 
      //      title="From storage" 
      //      color="#841584"
      //      onPress={() => null}
      //    />
      //  </View>
      //)
    }
  }

  async componentWillMount() {
    const { status } = await Permissions.askAsync(Permissions.CAMERA);
    this.setState({ hasCameraPermission: status === 'granted' });
  }

  render() {
    const { hasCameraPermission } = this.state;
    if (hasCameraPermission === null) {
      return <View />;
    } else if (hasCameraPermission === false) {
      return <Text>No access to camera</Text>;
    } else {
      return (
        <View style={{ flex: 1 }}>
          <Camera style={{ flex: 1 }} type={this.state.type}>
            <View
              style={{
                flex: 1,
                backgroundColor: 'transparent',
                flexDirection: 'row',
              }}>
              <TouchableOpacity
                style={{
                  flex: 1,
                  flexDirection: 'column',
                  alignSelf: 'flex-end',
                  alignItems: 'flex-end',
                }}
                onPress={() => {
                  this.setState({
                    type: this.state.type === Camera.Constants.Type.back
                      ? Camera.Constants.Type.front
                      : Camera.Constants.Type.back,
                  });
                }}>
                {/* <Text
                  style={{ fontSize: 18, marginBottom: 10, color: 'white' }}>
                  {' '}Flip{' '}
                </Text> */}
                <View style={{flexDirection: 'row', height: 40, }}>
                  {/* <Button onPress={()=>null} title="Flip" style={{alignSelf: 'flex-end',}} /> */}
                  <Button onPress={()=>null} title="Snap" style={{alignSelf: 'center', height: 40, }} />
                </View>
              </TouchableOpacity>
            </View>
          </Camera>
        </View>
      );
    }
  }
}
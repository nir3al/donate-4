import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  View,
  FlatList,
  Button,
  StatusBar,
} from 'react-native';

import { getAllDonations } from '../api/Donation'

import { Btn } from '../components/Btn';
import colors from '../constants/Colors';

export default class DonationListScreen extends React.Component {

  static navigationOptions = ({navigation}) => {
    return {
      headerStyle: { 
        marginTop: -1 * StatusBar.currentHeight,
        backgroundColor: colors.appHeaderBgColor,
      },
      title: "Donations",
      headerLeft: null,      
      headerRight: (
        <View style={{paddingRight: 15}}>
          <Btn 
            title="New donation" 
            color={colors.btnBgColor}
            fgColor="#333"
            onPress={() => {
              navigation.navigate('Donate');
            }}
          />
        </View>
        )
    }
  }
  
  state = {
    data: [],
    today: '',
    // isDataLoaded: false,
  }

  componentDidMount() {
    // this.props.navigation.setParams({ handleNewRequest: this.gotoNewRequest });
    getAllDonations().then(_data => {
      _data.forEach(d => {
        d.key = d._id
        d.date = Object.keys(d.dates)[0]
        let addressItems = (d.address||'').split(',')
        d.locName = addressItems[1] || addressItems[0] || d.address
      })
      let arr1 = _data.filter(x => x.name)
      let arr2 = _data.filter(x => !x.name)
      arr1.sort( (x, y) => x.date > y.date )
      arr2.sort( (x, y) => x.date > y.date )
      let data = arr1.concat(arr2)
      this.setState({data})
    })
  }

  // _data = [
  //   { date:"2018-1-23", amount: "5000", name: "Emma Watson", location: "Dehiwala", key: 1 },
  //   { date:"2017-12-20", amount: "10000", name: "Morgan Freeman", location: "Anuradhapura", key: 2 },
  //   { date:"2017-12-10", amount: "3000", name: "99x technology", location: "Jaffna", key: 3 },
  //   { date:"2017-12-2", amount: "1000", name: "Jason Statam", location: "Moratuwa", key: 4 },
  //   { date:"2017-11-31", amount: "5000", name: "WSO2", location: "Mathara", key: 5 },
  //   { date:"2017-11-20", amount: "5000", name: "Microsoft", location: "Badulla", key: 6 },
  //   { date:"2017-11-11", amount: "5000", name: "LSEG", location: "Jaffna", key: 7 },
  //   { date:"2017-11-2", amount: "5000", name: "Steven Crowder", location: "Mathara", key: 8 },
  //   { date:"2017-10-30", amount: "5000", name: "Hunter Avallone", location: "Kurunagala", key: 9 },
  //   { date:"2017-10-30", amount: "5000", name: "Lauren Southern", location: "Anuradhapura", key: 10 },
  // ];

  _renderItem = (item)=>(
    <View style={styles.bigListItem}>
      <View style={styles.bigListItemAllTextContainer}> 
        <Text style={styles.bigListItemHeader}>{item.name}</Text>
        <Text style={styles.bigListItemSubtext}>
          for {item.locName || '▭'} on {item.date || (item.dates||[])[0] || '▭'}
        </Text>
      </View>
      <View style={styles.bigListItemButtonsContainer}>
        <Btn
          title="View"
          bgColor={colors.btnBgColor} fgColor={colors.btnFgColor}
          onPress={(x)=> this.props.navigation.navigate("ViewDonation", {item, loggedIn:true,})}
        />
      </View>
    </View>
  );

  render() {
    // if (!this.props.navigation.state.params || !this.props.navigation.state.params.loggedIn) {
    //   return (
    //     <View style={{
    //       flex: 1,
    //       padding: 20,
    //     }}>
    //       <Text style={{
    //         textAlign: "center",
    //       }}>Please sign in to use this page.</Text>
    //     </View>
    //   );
    // }

    return (this.state.data) ? (<View style={styles.container}>
      <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
        <View>
          <FlatList
            style={styles.bigList}
            data={this.state.data}
            renderItem={({item})=>this._renderItem(item)}
          />
        </View>
      </ScrollView>
    </View>) : (<View>
      <Text style={styles.centeredText}>Loading data...</Text>
    </View>)
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.appBgColor,
  },
  contentContainer: {
    padding: 20,
    paddingTop: 10,
    paddingHorizontal: 0,
  },
  bigList: {
    paddingHorizontal: 0,
  },
  bigListItem: {
    height: 55,
    paddingHorizontal: 20,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: colors.appLinesColor,
    flexDirection:'row',
    justifyContent:'space-between',
  },
  bigListItemHeader: {
    flexDirection:'row',
    fontSize: 20,
    paddingTop: 3,
    fontWeight: '600',
    color: '#eee',
  },
  bigListItemSubtext: {
    flexDirection:'row',
    fontSize: 14,
    paddingBottom: 7,
    color: colors.appH2Color,
  },
  bigListItemAllTextContainer: {
    flexDirection:'column',
    justifyContent:'flex-start',
  },
  bigListItemButtonsContainer: {
    flexDirection:'column',
    justifyContent: 'center',
    height: 55,
  },
  centeredText: {
    textAlign: 'center',
  },
});

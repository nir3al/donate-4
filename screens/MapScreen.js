import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  StatusBar,
  Button,
  Image,
  Alert,
} from 'react-native';

import { MapView } from 'expo';

export default class MapScreen extends Component {

  static navigationOptions = ({navigation}) => {
    return {
      headerStyle: { marginTop: -1 * StatusBar.currentHeight },
      title: "Location",
      //headerRight: (
      //  <View style={{paddingRight: 15}}>
      //    <Button 
      //      title="By description" 
      //      color="#841584"
      //      onPress={() => navigation.navigate("LocationPicker", {loggedIn:true,})}
      //    />
      //  </View>
      //)
    }
  }

  state = {
    location: null,
    lat: null,
    long: null,
    locationName: null,
    locationId: null,
    locationDescription: null,
    placeId: null,
    errorMessage: null,
  };

  render() {
    // TODO: enable pinning a location
    return (
      <MapView
        style={{ flex: 1 }}
        initialRegion={{
          latitude: 6.9147,
          longitude: 79.9733,
          latitudeDelta: 0.02022,
          longitudeDelta: 0.01021,
        }}
      />
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
});
import React from 'react';
import {
  Image,
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
} from 'react-native';

import { MonoText } from '../components/StyledText';

import { logIn } from '../api/Auth'
import { APP_VER } from '../constants/Env'

import { Btn } from '../components/Btn';
import colors from '../constants/Colors';

export default class LoginScreen extends React.Component {
  static navigationOptions = {
    header: null,
    // tabBarVisible: false,
    title: "Log out",
  };

  static navigatorStyle = {
    tabBarHidden: true,
  }

  state = {
    email: null,
    pass: null,
    isBusy: false,
  }

  render() {
    return (
      <View style={styles.container}>
        <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
          <View style={styles.formContainer}>
            {/* <Text style={styles.txt}>{APP_VER}</Text> */}
            <Image
              style={{ width: 172, height: 44 }}
              source={require('../assets/images/Donate-lk-text-1.png')}
            />
            <Text style={styles.formHeader}>Log in</Text>
            <View style={{ margin: 9 }} />
            <Text style={{ color: '#ccc' }}>
              Please log in to access all features.
            </Text>
            <View style={{ margin: 12 }} />
            <TextInput value={this.state.email} onChangeText={(email) => this.setState({ email })} placeholder='E-mail' style={styles.loginInput} underlineColorAndroid='transparent' placeholderTextColor='#aaa' />
            <TextInput value={this.state.pass} onChangeText={(pass) => this.setState({ pass })} placeholder='Password' secureTextEntry={true} style={styles.loginInput} underlineColorAndroid='transparent' placeholderTextColor='#aaa' />
            <View style={{ margin: 9 }} />
            <Btn
              onPress={(x) => {
                if (this.state.email && this.state.pass) {
                  this.setState({ isBusy: true })
                  logIn(this.state.email, this.state.pass)
                    .then(userObj => {
                      console.log('logged in user:', userObj)
                      if (userObj) {
                        if (userObj.isAdmin) {
                          this.props.navigation.navigate("Main", { user: userObj })
                        } else {
                          console.log(userObj)
                          if (userObj.lastRequest) {
                            this.props.navigation.navigate("PendingUser", { user: userObj })
                          } else {
                            this.props.navigation.navigate("User", { user: userObj })
                          }
                        }
                        // global.
                        global.__user = userObj
                        this.setState({ isBusy: false })
                        // await AsyncStorage.setItem('@$:user', userObj)
                      } else {
                        //TODO: Show username/password mismatch error
                        this.setState({ isBusy: false })
                      }
                    }).catch(err => {
                      //TODO: Show username/password mismatch error
                      this.setState({ isBusy: false })
                    })
                }
              }}
              title="Log in"
              color="#1982C4"
              fgColor="#ffffff"
            />
            <View style={{ margin: 12 }} />
            <Text style={{ color: '#ccc' }}>
              If you don't already have an account, create one.
            </Text>
            <View style={{ margin: 9 }} />
            <Btn
              onPress={(x) => { this.props.navigation.navigate("Signup") }}
              title="Sign up"
              color="#6A4C93"
              fgColor="#ffffff"
            />
          </View>
        </ScrollView>
        <View style={styles.tabBarInfoContainer}>
          <Text style={{ paddingHorizontal: 10, color: '#ccc' }}>Contact us if you have any problems signing up.</Text>

          <View>
            <MonoText style={styles.codeHighlightText}>support@donate.lk</MonoText>
          </View>
        </View>
        {(this.state.isBusy) ? <View style={styles.overlay}></View> : null}
      </View>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#333',
  },
  contentContainer: {
    padding: 20,
    paddingTop: 30,
  },
  txt: {
    color: '#fff',
  },
  formContainer: {
    paddingVertical: 20,
    alignItems: 'center',
  },
  formHeader: {
    fontSize: 30,
    color: '#fff',
  },
  loginInput: {
    fontSize: 16,
    padding: 5,
    lineHeight: 22,
    minWidth: 150,
    textAlign: 'center',
    color: '#fff',
    backgroundColor: '#ffffff33',
    borderRadius: 4,
    margin: 3,
  },
  codeHighlightText: {
    color: 'rgba(255, 255, 255, 0.8)',
  },
  tabBarInfoContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    ...Platform.select({
      ios: {
        shadowColor: 'black',
        shadowOffset: { height: -3 },
        shadowOpacity: 0.1,
        shadowRadius: 3,
      },
      android: {
        elevation: 20,
      },
    }),
    alignItems: 'center',
    backgroundColor: '#fbfbfb',
    paddingVertical: 20,
    backgroundColor: '#333',
  },
  overlay: {
    flex: 1,
    backgroundColor: 'black',
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    opacity: 0.3
  },
});

import React from 'react';
import {
  Platform,
  ScrollView,
  StyleSheet,
  Text,
  View,
  Button,
  StatusBar,
  TextInput,
  Picker,
  Alert,
  Image,
} from 'react-native';

import { Constants, MapView, Location, Permissions, WebBrowser } from 'expo';

import MapViewDirections from 'react-native-maps-directions';
import getDirections from 'react-native-google-maps-directions'

import { GOOGLE_MAPS_DIRECTIONS_API_KEY } from '../constants/Env'

import { getAllWarnings } from '../api/Warning'
// import { getRoute } from '../api/Directions';

import { Btn } from '../components/Btn';
import colors from '../constants/Colors';
import { Directions } from '../components/Directions';

export default class DirectionsScreen extends React.Component {

  state = {
      origin: {latitude: 37.3318456, longitude: -122.0296002, latitudeDelta: 0.00922, longitudeDelta: 0.00421},
      destination: {latitude: 37.771707, longitude: -122.4053769},
      isLocationLoaded: false,
      areWarningsLoaded: false,

      coordinates: null,
    __coordinates: null,
    distance: null,
    duration: null,
    __durations: [],
    __duration_coords: [],
  };

  static navigationOptions = ({navigation}) => {
    return {
      headerStyle: { marginTop: -1 * StatusBar.currentHeight },
      tabBarVisible: true,
      title: "Directions",
    //   headerRight: (<View style={{paddingRight: 15}}>
    //       <Btn 
    //         title="Google map" 
    //         color="#84158455"
    //         fgColor={colors.btnFgColor}
    //         onPress={() => {
    //             //   
    //             const data = {
    //                 destination: {
    //                   latitude: navigation.state.params.lat,
    //                   longitude: navigation.state.params.long,
    //                   latitudeDelta: 0.00922, longitudeDelta: 0.00421
    //                 },
    //                 params: [
    //                   {
    //                     key: "dirflg",
    //                     value: "w"
    //                   }
    //                 ]
    //               }
              
    //             getDirections(data)
    //         }}
    //       />
    //     </View>),
    }
  }

  componentDidMount() {
    // this._mounted = true;
    // this.fetchAndRenderRoute();
}  

    resetState = (cb = null) => {
		this._mounted && this.setState({
			coordinates: null,
			distance: null,
			duration: null,
		}, cb);
    }
    
    decode(t, e) {
		for (var n, o, u = 0, l = 0, r = 0, d = [], h = 0, i = 0, a = null, c = Math.pow(10, e || 5); u < t.length;) {
			a = null, h = 0, i = 0;
			do a = t.charCodeAt(u++) - 63, i |= (31 & a) << h, h += 5; while (a >= 32);
			n = 1 & i ? ~(i >> 1) : i >> 1, h = i = 0;
			do a = t.charCodeAt(u++) - 63, i |= (31 & a) << h, h += 5; while (a >= 32);
			o = 1 & i ? ~(i >> 1) : i >> 1, l += n, r += o, d.push([l / c, r / c]);
		}

		return d = d.map(function(t) {
			return {
				latitude: t[0],
				longitude: t[1],
			};
		});
    }
    
    fetchAndRenderRoute = () => {

        console.log('fetching direction routes')

		let {
			origin,
			destination,
			waypoints,
			apikey,
			onStart,
			onReady,
			onError,
			mode = 'driving',
			language = 'en',
			directionsServiceBaseUrl = 'https://maps.googleapis.com/maps/api/directions/json',
			alternatives = true,
		} = {   
            origin:this.state.origin,
            destination:this.state.destination,
            apikey:GOOGLE_MAPS_DIRECTIONS_API_KEY,
            strokeWidth:3,
            strokeColor:"#357ff4",
            mode:"driving" 
        };

		if (!origin || !destination) {
			return;
		}

		if (origin.latitude && origin.longitude) {
			origin = `${origin.latitude},${origin.longitude}`;
		}

		if (destination.latitude && destination.longitude) {
			destination = `${destination.latitude},${destination.longitude}`;
		}

		if (!waypoints || !waypoints.length) {
			waypoints = '';
		} else {
			waypoints = waypoints
				.map(waypoint => (waypoint.latitude && waypoint.longitude) ? `${waypoint.latitude},${waypoint.longitude}` : waypoint)
				.join('|');
		}

		onStart && onStart({
			origin,
			destination,
			waypoints: waypoints ? waypoints.split('|') : [],
		});

		this.fetchRoute(directionsServiceBaseUrl, origin, waypoints, destination, apikey, mode, language, alternatives)
			.then(result => {
				if (!this._mounted) return;
				this.setState(result);
				onReady && onReady(result);
			})
			.catch(errorMessage => {
				this.resetState();
				console.warn(`MapViewDirections Error: ${errorMessage}`); // eslint-disable-line no-console
				onError && onError(errorMessage);
			});
	}

	fetchRoute(directionsServiceBaseUrl, origin, waypoints, destination, apikey, mode, language, alternatives) {

		// Define the URL to call. Only add default parameters to the URL if it's a string.
		let url = directionsServiceBaseUrl;
		if (typeof (directionsServiceBaseUrl) === 'string') {
            // url += `?origin=${origin}&waypoints=${waypoints}&destination=${destination}&key=${apikey}&mode=${mode}&language=${language}&alternatives=${alternatives}`;
            url += `?origin=${origin}&destination=${destination}&key=${apikey}&mode=${mode}&language=${language}&alternatives=${alternatives}`;
		}

		return fetch(url)
			.then(response => response.json())
			.then(json => {

				if (json.status !== 'OK') {
					const errorMessage = json.error_message || 'Unknown error';
					return Promise.reject(errorMessage);
				}

				if (json.routes.length) {

                    const route = json.routes[0];
                    let routesList = [];
                    if (json.routes[0]) routesList.push(json.routes[0]);
                    console.log(routesList);
                    if (json.routes[1]) routesList.push(json.routes[1]);
                    if (json.routes[2]) routesList.push(json.routes[2]);

					return Promise.resolve({
						distance: route.legs.reduce((carry, curr) => {
							return carry + curr.distance.value;
						}, 0) / 1000,
						duration: route.legs.reduce((carry, curr) => {
							return carry + curr.duration.value;
						}, 0) / 60,
						coordinates: this.decode(route.overview_polyline.points),
                        __coordinates: routesList.map(r => this.decode(r.overview_polyline.points)),
                        __durations: routesList.map(r => r.legs.reduce((carry, curr) => {
                            console.log('duration calc:', carry, curr.duration.value)
							return carry + curr.duration.value;
                        }, 0) / 60),
                        __duration_coords: routesList.map(r => this.getRouteLabelLocation(r.legs))
					});

				} else {
					return Promise.reject();
				}
			});
    }

    getRouteLabelLocation(legs) {
        let coords = legs[0].steps[0].start_location; // {lat,lng}

        for (let i = 1; i < 6; i++) {
            const element = legs[0].steps[i];

            if (element) {
                coords = element.start_location;
            }
            else {
                break;
            }
        }

        return coords;
    }
    
    createDirectionPathItems() {
        console.log('getting directions');

		if (!this.state.__coordinates) {
			return null;
		}

		const {
			origin, // eslint-disable-line no-unused-vars
			waypoints, // eslint-disable-line no-unused-vars
			destination, // eslint-disable-line no-unused-vars
			apikey, // eslint-disable-line no-unused-vars
			onReady, // eslint-disable-line no-unused-vars
			onError, // eslint-disable-line no-unused-vars
			mode, // eslint-disable-line no-unused-vars
			language, // eslint-disable-line no-unused-vars
			...props
        } =
        {   origin:this.state.origin,
            destination:this.state.destination,
            apikey:GOOGLE_MAPS_DIRECTIONS_API_KEY,
            strokeWidth:3,
            strokeColor:"#357ff4",
            mode:"driving" 
        }
        // this.props;

        let i = 0;

        console.log(this.state.__coordinates[0]);

		let x = 
			// <MapView.Polyline coordinates={this.state.coordinates} {...props} />
			this.state.__coordinates.map(coords => <MapView.Polyline coordinates={coords} {...props} key={i++} />)
        ;
        
        console.log(x.length);
        // console.log(x);

        return x;
	}

  async componentWillMount() {
    this._mounted = false;

        await this._loadWarningsAsync();

        const destination = {
            latitude: this.props.navigation.state.params.lat,
            longitude: this.props.navigation.state.params.long
        };
        this.setState({ destination });

        await this._loadLocationAsync();
        this.setState({isLocationLoaded:true, areWarningsLoaded:true});
        
        // // this.setState({path1a: {latitude: this.state.destination.latitude - 0.00001, longitude:this.state.destination.longitude - 0.0001}});
        
        // // getRoute(this.state.origin, this.state.destination)
        // //     .then(coords => {
        // //         this.setState({ coords });
        // //         console.log(this.state.coords);
        // //     });

        // this.setState({
        //     point0a:{
        //         latitude: this.state.origin.latitude + 0.0009,
        //         longitude: this.state.origin.longitude + 0.004,
        //     },
        //     point0b:{
        //         latitude: this.state.destination.latitude + 0.0008,
        //         longitude: this.state.destination.longitude + 0.006,
        //     },
        //     point1a:{
        //         latitude: this.state.origin.latitude - 0.0008,
        //         longitude: this.state.origin.longitude - 0.005,
        //     },
        //     point2a:{
        //         latitude: this.state.destination.latitude - 0.0008 ,
        //         longitude: this.state.destination.longitude - 0.004,
        //     },
        //     point3a:{
        //         latitude: this.state.destination.latitude - 0.0008 ,
        //         longitude: this.state.destination.longitude - 0.004,
        //     },
        // });
        
        console.log(this.state.origin);

        this._mounted = true;
        this.fetchAndRenderRoute();
  }

  createRouteLabels() {
      if (!this.state.__durations || this.state.__durations.length == 0) {
          return null;
        }

        console.log('durations:', this.state.__duration_coords, this.state.__durations);
        
    let i = 0;
      return this.state.__durations.map(dur => <MapView.Marker
        key={i}
        coordinate={{latitude:+this.state.__duration_coords[i].lat, longitude:+this.state.__duration_coords[i++].lng}}
        >
            <View style={{
                backgroundColor: '#5500aa',
                borderRadius: 3,
                padding: 2,
                borderColor: '#ffffff',
                borderWidth: 2,
            }}>
                <Text style={{
                    color: '#ffffff'
                }}>{Math.trunc(dur)+''} min</Text>
            </View>
        </MapView.Marker>)
        // title="Your destination"
        // description={this.props.navigation.state.params.address}
  }

  render() {
    return (this.state.isLocationLoaded) ? 
        (<View style={styles.container}>
            <MapView initialRegion={this.state.origin} style={styles.map}>
                {(this.state.areWarningsLoaded) ?
                    this.state.markers.map(marker => (
                        <MapView.Circle
                            key={marker.key}
                            center={marker}
                            radius={500}
                            fillColor="rgba(150, 0, 0, 0.4)"
                            strokeColor="rgba(150, 0, 0, 0.1)"
                        />
                    ))
                : null
                }
                <MapView.Marker
                    coordinate={this.state.origin}
                    title="You're here"
                    description="Use heatmap to avoid danger areas."
                />
                <MapView.Marker
                    coordinate={this.state.destination}
                    title="Your destination"
                    description={this.props.navigation.state.params.address}
                />

                {this.createRouteLabels()}

                {this.createDirectionPathItems()}

                {/* <Directions
                    origin={this.state.origin}
                    destination={this.state.destination}
                    apikey={GOOGLE_MAPS_DIRECTIONS_API_KEY}
                    strokeWidth={3}
                    strokeColor="#357ff4"
                    mode="driving"
                /> */}

                {/* <MapViewDirections
                    origin={this.state.origin}
                    destination={this.state.destination}
                    apikey={GOOGLE_MAPS_DIRECTIONS_API_KEY}
                    strokeWidth={3}
                    strokeColor="#357ff4"
                    mode="driving"
                /> */}

                {/* <MapViewDirections
                    origin={this.state.origin}
                    destination={this.state.point0a}
                    apikey={GOOGLE_MAPS_DIRECTIONS_API_KEY}
                    strokeWidth={3}
                    strokeColor="#357ff4"
                    mode="driving"
                />
                <MapViewDirections
                    origin={this.state.point0a}
                    destination={this.state.point0b}
                    apikey={GOOGLE_MAPS_DIRECTIONS_API_KEY}
                    strokeWidth={3}
                    strokeColor="#357ff4"
                    mode="driving"
                />
                <MapViewDirections
                    origin={this.state.point0b}
                    destination={this.state.destination}
                    apikey={GOOGLE_MAPS_DIRECTIONS_API_KEY}
                    strokeWidth={3}
                    strokeColor="#357ff4"
                    mode="driving"
                />

                <MapViewDirections
                    origin={this.state.origin}
                    destination={this.state.point1a}
                    apikey={GOOGLE_MAPS_DIRECTIONS_API_KEY}
                    strokeWidth={3}
                    strokeColor="#357ff4"
                    mode="driving"
                />
                <MapViewDirections
                    origin={this.state.point1a}
                    destination={this.state.destination}
                    apikey={GOOGLE_MAPS_DIRECTIONS_API_KEY}
                    strokeWidth={3}
                    strokeColor="#357ff4"
                    mode="driving"
                />

                <MapViewDirections
                    origin={this.state.origin}
                    destination={this.state.point2a}
                    apikey={GOOGLE_MAPS_DIRECTIONS_API_KEY}
                    strokeWidth={3}
                    strokeColor="#357ff4"
                    mode="driving"
                />
                <MapViewDirections
                    origin={this.state.point2a}
                    destination={this.state.destination}
                    apikey={GOOGLE_MAPS_DIRECTIONS_API_KEY}
                    strokeWidth={3}
                    strokeColor="#357ff4"
                    mode="driving"
                /> */}

                {/* <MapView.Polyline
                    coordinates={[
                        this.state.origin, // optional
                        ...this.state.coords,
                        this.state.destination, // optional
                    ]}
                    strokeWidth={4}
                /> */}
            </MapView>
        </View>) 
        : 
        (<View style={styles.container}>
        <Text style={styles.centeredText}>Loading your location and warnings...</Text>
      </View>)
  }

    _loadLocationAsync = async () => {
        let { status } = await Permissions.askAsync(Permissions.LOCATION);
        if (status !== 'granted') {
            this.setState({
                errorMessage: 'Permission to access location was denied',
            });
        }

        let location = await Location.getCurrentPositionAsync({});

        if (location) {
            this.setState({
                location,
                origin: {
                    latitude: location.coords.latitude,
                    longitude: location.coords.longitude,
                    latitudeDelta: this.state.origin.latitudeDelta,
                    longitudeDelta: this.state.origin.longitudeDelta,
                }
            });
        }

        return location;
    };

    _loadWarningsAsync = async () => {
        const warnings = await getAllWarnings()
        warnings.forEach(w => w.key = w._id)
        this.setState({markers:warnings})
        return warnings
    }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  map: {
      flex: 1,
      marginBottom: -25,
  },
  centeredText: {
      textAlign: 'center',
      marginTop: 10,
  },
});

import React from 'react';
import { Platform, AsyncStorage } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { TabNavigator, TabBarBottom } from 'react-navigation';

import Colors from '../constants/Colors';

import HomeScreen from '../screens/HomeScreen';
import LinksScreen from '../screens/LinksScreen';
import SettingsScreen from '../screens/SettingsScreen';
import RequestListScreen from '../screens/RequestListScreen';
import PendingRequestScreen from '../screens/PendingRequestScreen';
import LoginScreen from '../screens/LoginScreen';
import DonationListScreen from '../screens/DonationListScreen';
import CreateWarningScreen from '../screens/CreateWarningScreen'

export default TabNavigator(
  {
    PendingRequest: {
      screen: PendingRequestScreen,
    },
    CreateWarning: {
      screen: CreateWarningScreen,
    },
    Logout: {
      screen: LoginScreen,
    },
  },
  {
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused }) => {
        const { routeName } = navigation.state;
        let iconName;
        switch (routeName) {
          case 'Logout':
            iconName = Platform.OS === 'ios' ? `ios-log-out${focused ? '' : '-outline'}` : 'md-log-out';
            break;
          case 'PendingRequest':
            iconName = Platform.OS === 'ios' ? `ios-bookmark${focused ? '' : '-outline'}` : 'md-bookmark';
            break;
          case 'CreateWarning':
            iconName = Platform.OS === 'ios' ? `ios-nuclear${focused ? '' : '-outline'}` : 'md-bookmark';
            break;
        }
        return (
          <Ionicons
            name={iconName}
            size={28}
            style={{ marginBottom: -3 }}
            color={focused ? Colors.tabIconSelected : Colors.tabIconDefault}
          />
        );
      },
    }),
    tabBarComponent: TabBarBottom,
    tabBarPosition: 'bottom',
    animationEnabled: false,
    swipeEnabled: false,
    tabBarOptions:{
      //other properties
      // pressColor: 'gray',//for click (ripple) effect color
      style: {
        backgroundColor: '#665A77',//color you want to change
      },
      labelStyle: {
        color: '#fff',
        // borderRadius: 100,
        // padding: 6,
        // paddingLeft: 16,
        // paddingRight: 16,
        // margin: 0,
      },
    },
  }
);

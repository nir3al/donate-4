import { Notifications } from 'expo';
import React from 'react';
import { StackNavigator } from 'react-navigation';

import MainTabNavigator from './MainTabNavigator';
import UserTabNavigator from './UserTabNavigator';
import PendingUserTabNavigator from './PendingUserTabNavigator';

import LocationPickerScreen from '../screens/LocationPickerScreen';
import LocationPicker2Screen from '../screens/LocationPicker2Screen';
import ViewRequestPhotoScreen from '../screens/ViewRequestPhotoScreen'
import MapScreen from '../screens/MapScreen';
import DirectionMapScreen from '../screens/DirectionMapScreen';
import CameraScreen from '../screens/CameraScreen';
import DonateScreen from '../screens/DonateScreen';
import ViewRequestScreen from '../screens/ViewRequestScreen';
import LoginScreen from '../screens/LoginScreen';
import SignupScreen from '../screens/SignupScreen';
import WeatherScreen from '../screens/WeatherScreen';
import ViewDonationScreen from '../screens/ViewDonationScreen';
import RequestStatusScreen from '../screens/RequestStatusScreen';
import RequestListScreen from '../screens/RequestListScreen';
import DirectionsScreen from '../screens/DirectionsScreen'
import AcceptRequestScreen from '../screens/AcceptRequestScreen';
import DonationListScreen from '../screens/DonationListScreen';
import NewRequestScreen from '../screens/NewRequestScreen';

import registerForPushNotificationsAsync from '../api/registerForPushNotificationsAsync';

// global.__user = {}

const RootStackNavigator = StackNavigator(
  {
    // NewRequestScreen: {
    //   screen: NewRequestScreen,
    // },
    // RequestList: {
    //   screen: RequestListScreen,
    //   // visible: await AsyncStorage.getItem('@$:user')
    // },
    // Donate: {
    //   screen: DonateScreen,
    // },
    // LocationPicker2: {
    //   screen: LocationPicker2Screen,
    // },
    Login: {
      screen: LoginScreen,
    },
    Main: {
      screen: MainTabNavigator,
    },
    User: {
      screen: UserTabNavigator,
    },
    PendingUser: {
      screen: PendingUserTabNavigator,
    },
    ViewRequestPhoto: {
      screen: ViewRequestPhotoScreen,
    },
    // Login: {
    //   screen: LoginScreen,
    // },
    Weather: {
      screen: WeatherScreen,
    },
    Signup: {
      screen: SignupScreen,
    },
    Donate: {
      screen: DonateScreen,
    },
    ViewDonation: {
      screen: ViewDonationScreen,
    },
    Map: {
      screen: MapScreen,
    },
    ViewRequest: {
      screen: ViewRequestScreen,
    },
    DirectionMap: {
      screen: DirectionMapScreen,
    },
    // LocationPicker: {
    //   screen: LocationPickerScreen,
    // },
    LocationPicker2: {
      screen: LocationPicker2Screen,
    },
    Camera: {
      screen: CameraScreen,
    },
    RequestStatusScreen: {
      screen: RequestStatusScreen,
    },
    Directions: {
      screen: DirectionsScreen,
    },
    AcceptRequest: {
      screen: AcceptRequestScreen,
    },
  },
  {
    navigationOptions: () => ({
      headerTitleStyle: {
        fontWeight: 'normal',
      },
    }),
  }
);

export default class RootNavigator extends React.Component {
  componentDidMount() {
    this._notificationSubscription = this._registerForPushNotifications();
  }

  componentWillUnmount() {
    this._notificationSubscription && this._notificationSubscription.remove();
  }

  render() {
    return <RootStackNavigator />;
  }

  _registerForPushNotifications() {
    // Send our push token over to our backend so we can receive notifications
    // You can comment the following line out if you want to stop receiving
    // a notification every time you open the app. Check out the source
    // for this function in api/registerForPushNotificationsAsync.js

    // registerForPushNotificationsAsync();

    // Watch for incoming notifications

    // this._notificationSubscription = Notifications.addListener(this._handleNotification);
  }

  _handleNotification = ({ origin, data }) => {
    console.log(`Push notification ${origin} with data: ${JSON.stringify(data)}`);
  };
}

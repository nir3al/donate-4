import React from 'react';
import { Platform, AsyncStorage } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import { TabNavigator, TabBarBottom } from 'react-navigation';

import Colors from '../constants/Colors';

import HomeScreen from '../screens/HomeScreen';
import LinksScreen from '../screens/LinksScreen';
import SettingsScreen from '../screens/SettingsScreen';
import RequestListScreen from '../screens/RequestListScreen';
import NewRequestScreen from '../screens/NewRequestScreen';
import LoginScreen from '../screens/LoginScreen';
import DonationListScreen from '../screens/DonationListScreen';

export default TabNavigator(
  {
    RequestList: {
      screen: RequestListScreen,
      // visible: await AsyncStorage.getItem('@$:user')
    },
    DonationList: {
      screen: DonationListScreen,
      // visible: await AsyncStorage.getItem('@$:user')
    },
    // NewRequest: {
    //   screen: NewRequestScreen,
    // },
    Logout: {
      screen: LoginScreen,
    },
  },
  {
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused }) => {
        const { routeName } = navigation.state;
        let iconName;
        switch (routeName) {
          case 'RequestList':
            iconName = Platform.OS === 'ios' ? `ios-folder${focused ? '' : '-outline'}` : 'md-folder';
            break;
          case 'Home':
            iconName =
              Platform.OS === 'ios'
                ? `ios-information-circle${focused ? '' : '-outline'}`
                : 'md-information-circle';
            break;
          // case 'Links':
          //   iconName = Platform.OS === 'ios' ? `ios-link${focused ? '' : '-outline'}` : 'md-link';
          //   break;
          case 'Logout':
            iconName = Platform.OS === 'ios' ? `ios-log-out${focused ? '' : '-outline'}` : 'md-log-out';
            break;
          case 'NewRequest':
            iconName = Platform.OS === 'ios' ? `ios-send${focused ? '' : '-outline'}` : 'md-send';
            break;
          case 'DonationList':
            iconName = Platform.OS === 'ios' ? `ios-cash${focused ? '' : '-outline'}` : 'md-cash';
            break;
          case 'LocationPicker':
            iconName = Platform.OS === 'ios' ? `ios-locate${focused ? '' : '-outline'}` : 'md-locate';
            break;
          case 'Map':
            iconName = Platform.OS === 'ios' ? `ios-map${focused ? '' : '-outline'}` : 'md-map';
            break;
          // case 'Settings':
          //   iconName =
          //     Platform.OS === 'ios' ? `ios-options${focused ? '' : '-outline'}` : 'md-options';
        }
        return (
          <Ionicons
            name={iconName}
            size={28}
            style={{ marginBottom: -3 }}
            color={focused ? Colors.tabIconSelected : Colors.tabIconDefault}
          />
        );
      },
    }),
    tabBarComponent: TabBarBottom,
    tabBarPosition: 'bottom',
    animationEnabled: false,
    swipeEnabled: false,
    tabBarOptions:{
      //other properties
      // pressColor: 'gray',//for click (ripple) effect color
      style: {
        backgroundColor: '#665A77',//color you want to change
      },
      labelStyle: {
        color: '#fff',
        // borderRadius: 100,
        // padding: 6,
        // paddingLeft: 16,
        // paddingRight: 16,
        // margin: 0,
      },
    },
  }
);
